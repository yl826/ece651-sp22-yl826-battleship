package ece651.sp22.yl826.battleship;

import java.util.HashMap;

/**
 * This class represents the basic type for a Ship.
 */
public abstract class BasicShip<T> implements Ship<T> {

    /**
     * The DisplayInfo to display this ship.
     */
    protected ShipDisplayInfo<T> myDisplayInfo;

    /**
     * Hold all coordinate that this ship occupied and track which one have been hit.
     */
    protected HashMap<Coordinate, Boolean> myPieces;


    /**
     * Hold all coordinate that this ship occupied and track its relative index.
     */
    protected HashMap<Coordinate, Integer> indexMap;


    /**
     * the info to display for enemy side of view.
     */
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
     * the upper left coordinate of this ship.
     */
    public Placement upperLeft;


    /**
     * Construct a BasicShip.
     *
     * @param upperLeft        is the placement of this ship.
     * @param where            is the coordinates occupied by this ship.
     * @param myDisplayInfo    is the DisplayInfo to display this ship.
     * @param enemyDisplayInfo is the enemy's DisplayInfo
     */
    public BasicShip(Placement upperLeft, HashMap<Coordinate, Integer> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        myPieces = new HashMap<>();
        for (Coordinate c : where.keySet()) {
            myPieces.put(c, false);
        }
        this.upperLeft = upperLeft;
        this.indexMap = where;
    }


    /**
     * Check if this ship occupies the given coordinate.
     *
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    /**
     * Check if this ship has been hit in all of its locations meaning it has been
     * sunk.
     *
     * @return true if this ship has been sunk, false otherwise.
     */
    @Override
    public boolean isSunk() {
        return !myPieces.containsValue(false);
    }

    /**
     * Make this ship record that it has been hit at the given coordinate. The
     * specified coordinate must be part of the ship.
     *
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    /**
     * Check if this ship was hit at the specified coordinates. The coordinates must
     * be part of this Ship.
     *
     * @param where is the coordinates to check.
     * @return true if this ship as hit at the indicated coordinates, and false
     * otherwise.
     * @throws IllegalArgumentException if the coordinates are not part of this
     *                                  ship.
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    /**
     * Return the view-specific information at the given coordinate. This coordinate
     * must be part of the ship.
     *
     * @param where  is the coordinate to return information for
     * @param myShip true if view by my ship side.
     * @return true if this ship as hit at the indicated coordinates, and false
     * otherwise.
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        //look up the hit status of this coordinate
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        } else {
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
        }
    }

    /**
     * check coordinate if is in this ship.
     *
     * @param c The coordinate need to check.
     * @throws IllegalArgumentException when the coordinate is not in this ship.
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!occupiesCoordinates(c))
            throw new IllegalArgumentException("The coordinate is not part of this ship");
    }

    /**
     * Get all the Coordinates that this Ship occupies.
     *
     * @return An Iterable with the coordinates that this Ship occupies
     */
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }


    /**
     * Get relative coordinate int of this ship
     *
     * @param c coordinate
     * @return the relative index of this coordinate.
     */
    public int getRelativeIndex(Coordinate c) {
        return indexMap.get(c);
    }

    /**
     * Get the coordinate by its relative index.
     *
     * @param index is the relative index of target coordinate
     * @return coordinate if there is a match with index, otherwise null
     */
    public Coordinate getCoordinateByRelativeIndex(int index) {
        for (Coordinate c : indexMap.keySet()) {
            if (indexMap.get(c) == index)
                return c;
        }
        return null;
    }


}
