package ece651.sp22.yl826.battleship;


import java.util.HashMap;

/**
 * This class represents the Carrier
 *
 * @param <T> Type of info to display with.
 */
public class Carrier<T> extends BasicShip<T> {
    /**
     * The name of this ship. Such as "submarine".
     */
    private final String name;

    /**
     * Generate all the coordinate where the rectangleShip takes based on the arguments.
     *
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @return A set of coordinate which contains all the coordinate of this rectangle ship.
     */
    static HashMap<Coordinate, Integer> makeCoords(Coordinate upperLeft, char orientation) {
        HashMap<Coordinate, Integer> myCoords = new HashMap<>();
        //Index folow by this order
        //0
        //1
        //2
        //34
        //45
        // 6
        if (orientation == 'U') {
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 0),0);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 0),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 0),3);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1),4);
            myCoords.put(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1),5);
            myCoords.put(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1),6);
        }
        if (orientation == 'R') {
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 1),3);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 2),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 3),1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 4),0);

            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),6);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),5);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2),4);
        }
        if (orientation == 'D') {
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 0),6);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),5);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 0),4);

            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),3);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1),1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1),0);
        }
        if (orientation == 'L') {
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 2),4);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 3),5);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 4),6);

            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),0);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 3),3);
        }

        return myCoords;
    }

    /**
     * Construct a rectangle ship with specified arguments.
     *
     * @param upperLeft       is the upper-left corner coordinate of this rectangle ship.
     * @param shipDisplayInfo is the info need to display this ship.
     */
    public Carrier(String name, Coordinate upperLeft, ShipDisplayInfo<T> shipDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, char orientation) {
        super(new Placement(upperLeft, orientation), makeCoords(upperLeft, orientation), shipDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Construct a rectangle ship with specified arguments.
     *
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param data      is the info need to display in normal condition.
     * @param onHit     is the display info when hit.
     */
    public Carrier(String name, Coordinate upperLeft, T data, T onHit, char orientation) {
        this(name, upperLeft, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data), orientation);
    }


    /**
     * Get the name of this Ship, such as "submarine".
     *
     * @return the name of this ship
     */
    @Override
    public String getName() {
        return name;
    }






}
