package ece651.sp22.yl826.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {
    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    /**
     * Make a battleship.
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        //return createShip(where, 1, 4, 'b', "Battleship");
        if(where.getOrientation()!='U'&&where.getOrientation()!='R'&&where.getOrientation()!='D'&&where.getOrientation()!='L'){
            throw new IllegalArgumentException("Invalid orientation for BattleShip: " + where.getOrientation());
        }
        return new BattleShip<>("Battleship", where.getWhere(), 'b', '*', where.getOrientation());

    }

    /**
     * Make a carrier.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        // return createShip(where, 1, 6, 'c', "Carrier");
        if(where.getOrientation()!='U'&&where.getOrientation()!='R'&&where.getOrientation()!='D'&&where.getOrientation()!='L'){
            throw new IllegalArgumentException("Invalid orientation for Carrier: " + where.getOrientation());
        }
        return new Carrier<>("Carrier", where.getWhere(), 'c', '*', where.getOrientation());

    }

    /**
     * Make a destroyer.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    /**
     * Create ship based on the specified information.
     *
     * @param where  A placement specify the coordinate and orientation of this ship.
     * @param w      The width of this ship.
     * @param h      The height of this ship.
     * @param letter The letter to display this ship.
     * @param name   The name of this ship.
     * @return The ship created based on the above information.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if(where.getOrientation()!='V'&&where.getOrientation()!='H'){
            throw new IllegalArgumentException("Invalid orientation for "+name+": " + where.getOrientation());
        }
        int width = w;
        int height = h;
        if (where.getOrientation() == 'H') {
            width = h;
            height = w;
        }
        return new RectangleShip<>(name, where.getWhere(), width, height, letter, '*');
    }


}
