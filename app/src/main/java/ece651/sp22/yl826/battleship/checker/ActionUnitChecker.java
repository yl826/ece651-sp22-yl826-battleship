package ece651.sp22.yl826.battleship.checker;

import ece651.sp22.yl826.battleship.order.Order;
import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.territory.Territory;
import ece651.sp22.yl826.battleship.unit.Unit;

public class ActionUnitChecker extends ActionChecker {

    /**
     * check if the unit related to this action is valid (exist, non-negative, sufficient amount)
     * @param riskMap
     * @param moveOrder
     * @return null if all rules passed; return error message if rule didn't pass
     */
    @Override
    protected String checkMyRule(RISKMap riskMap, Order moveOrder) {
        Territory src = riskMap.getTerritoryByName(moveOrder.getSrcTerritory());
        Unit unitToMove = src.getUnitByType(moveOrder.getUnitType());
        if (unitToMove == null) {
            return "You do not have " + moveOrder.getUnitType() + " in " + moveOrder.getSrcTerritory() + "!";
        }
        if (moveOrder.getUnitAmount() < 0) {
            return "Unit Amount must be positive integer!";
        }

        if (unitToMove.getAmount() < moveOrder.getUnitAmount()) {
            return "You do not have sufficient " + moveOrder.getUnitType() + " to move in " + moveOrder.getSrcTerritory() + "!";
        }
        return null;
    }

    public ActionUnitChecker(ActionChecker next) {
        super(next);
    }

}
