package ece651.sp22.yl826.battleship.player;

import ece651.sp22.yl826.battleship.*;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class TextPlayerAI extends TextPlayer {

    /**
     * The alphabet of row. such as ABCD.
     */
    final String rowAlphabet;

    /**
     * The alphabet of column. such as 0123.
     */
    final String columnAlphabet;

    /**
     * The alphabet of orientation. such as VUH.
     */
    final String orientationAlphabet;

    /**
     * The alphabet of action. such as FSM.
     */
    final String actionAlphabet;

    /**
     * The random of our AI.
     */
    final Random random;
    /**
     * Construct a  TextPlayer with specified information.
     *
     * @param name         is the name of this player.
     * @param theBoard     is the board of this player.
     * @param inputReader  is the input stream.
     * @param out          is the output stream.
     * @param shipFactory  is the shipFactory to create ship.
     * @param numMovesLeft is the number of moves we can do.
     * @param numSonarLeft is the number of sonar scan we can do.
     */
    public TextPlayerAI(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory, int numMovesLeft, int numSonarLeft) {
        super(name, theBoard, inputReader, out, shipFactory, numMovesLeft, numSonarLeft);
        rowAlphabet = generateRowAlphabet(theBoard);
        columnAlphabet = generateColumnAlphabet(theBoard);
        orientationAlphabet = generateOrientationAlphabet();
        actionAlphabet = generateActionAlphabet();
        random = new Random(42);
    }


    /**
     * Generate a random placement.
     * @return a random placement.
     */
    public String randomPlacement() {
        return randomCoordinate() + randomOrientation();
    }

    /**
     * Generate a random coordinate.
     * @return a random coordinate.
     */
    public String randomCoordinate() {
        return randomOneCharByAlphabet(rowAlphabet) + randomOneCharByAlphabet(columnAlphabet);
    }
    /**
     * Generate a random action.
     * @return a random action.
     */
    public String randomAction() {
        return randomOneCharByAlphabet(actionAlphabet);
    }

    /**
     * Generate a random orientation.
     * @return a random orientation.
     */
    public String randomOrientation() {
        return randomOneCharByAlphabet(orientationAlphabet);
    }

    /**
     * Generate a random char with given alphabet.
     * @param alphabet is the alphabet we need to generate from.
     * @return a random char.
     */
    public String randomOneCharByAlphabet(String alphabet) {
        return String.valueOf(alphabet.charAt(random.nextInt(alphabet.length())));
    }

    /**
     * Generate the row alphabet.
     * @param theBoard the board which we need to generate from.
     * @return an row alphabet.
     */
    public static String generateRowAlphabet(Board<Character> theBoard) {
        StringBuilder alphabet = new StringBuilder();
        for (int i = 'A'; i < 'A' + theBoard.getHeight(); i++) {
            alphabet.append((char) i);
        }
        return alphabet.toString();
    }

    /**
     * Generate the column alphabet.
     * @param theBoard the board which we need to generate from.
     * @return an column alphabet.
     */
    public static String generateColumnAlphabet(Board<Character> theBoard) {
        StringBuilder alphabet = new StringBuilder();
        for (int i = 0; i < theBoard.getWidth(); i++) {
            alphabet.append(i);
        }
        return alphabet.toString();
    }

    /**
     * Generate the orientation alphabet.
     * @return an orientation alphabet.
     */
    public static String generateOrientationAlphabet() {
        return "HVUDRL";
    }

    /**
     * Generate the action alphabet.
     * @return an action alphabet.
     */
    public static String generateActionAlphabet() {
        return "FMS";
    }

    /**
     * Read one placement command from input and new a Placement.
     *
     * @param prompt is a friendly advise for user.
     * @return The user-specific Placement for that input command.
     */
    @Override
    public Placement readPlacement(String prompt) {
        String s = randomPlacement();
        return new Placement(s);
    }

    /**
     * Display the empty board first, then the help message, finally the placement result.
     *
     */
    @Override
    public void doPlacementPhase()  {
        for (String singleShipToPlace : shipsToPlace) {

            doOnePlacement(singleShipToPlace, shipCreationFns.get(singleShipToPlace));
        }
    }

    /**
     * Do one placement by generate a random palcement.
     * @param shipName is the name of this ship need to placed.
     * @param createFn is the create function.
     */
    @Override
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn)  {
        boolean done = false;
        while (!done) {
            try {
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                String errorMessage = theBoard.tryAddShip(s);
                if (errorMessage != null) {
                    throw new IllegalArgumentException(errorMessage);
                }
                done = true;
            } catch (IllegalArgumentException e) {
                //out.print(e.getMessage() + "\n");
            }
        }
    }

    /**
     * Read a Coordinate from random generator.
     *
     * @param prompt is a friendly advise for user.
     * @return Coordinate read from input source.
     * @throws IllegalArgumentException if the line we read from input source is not a valid coordinate
     */
    @Override
    public Coordinate readCoordinate(String prompt) throws IllegalArgumentException {
        String s = randomCoordinate();
        return new Coordinate(s);
    }

    /**
     * Read an action from random generator. Then return this action.
     * Now we have action as follows:
     * F Fire at a square
     * M Move a ship to another square (2 remaining)
     * S Sonar scan (1 remaining)
     *
     * @return The action player want to do.
     * @throws IllegalArgumentException if the action not valid.
     */
    @Override
    public String readAction() throws  IllegalArgumentException {
        String s = randomAction();
        s = s.toUpperCase();

        return s;
    }

    /**
     * Play a single turn.
     * If there is any invalid input, just ask user to start over
     * from which action type the player wants to use.
     *
     * @param enemyBoard is the enemy board
     * @param enemyName  is the enemy name
     */
    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
        boolean successfullyPlayOneTurn = false;
        while (!successfullyPlayOneTurn) {
            try {
                tryPlayOneTurn(enemyBoard);
            } catch (IllegalArgumentException e) {
                //out.print(e.getMessage()+"\n");
                continue;
            }
            successfullyPlayOneTurn = true;

        }
    }

    /**
     * Move any ship to another location.
     * When using this move type, a player selects one of
     * his/her own ships (the game should prompt for which
     * ship, and the player should be able to type any
     * coordinate which is a part of the ship they want).
     * The player then is prompted for a new placement
     * (location + orientation, as in initial placement).
     * The ship is then moved to that position.  Any
     * existing damage to the ship remains in the
     * same relative position(s) of the ship.
     * <p>
     * If the player selects an invalid location,
     * the player is re-prompted for their actions (and
     * might select move again, or might select a different
     * action).
     * <p>
     * Note that the other player's display does NOT gain
     * any information that the ship moved.  If they
     * previously hit that ship, the hit markers remain.
     * If they previously missed the new location, those
     * markers also remain.
     *
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if any input information(the ship which player want to move,
     *                                  the destination where player want to move their ship to) is invalid
     */
    @Override
    public void tryMoveShip() throws IOException, IllegalArgumentException {
        if (numMovesLeft <= 0) {
            throw new IllegalArgumentException("All moves used. Choose a different option.");
        }

        Coordinate oriCoordinate = readCoordinate("Which ship do you want to move?\nEnter a coordinate:");

        Ship<Character> oriShip = theBoard.getShipAt(oriCoordinate);
        if (oriShip == null)
            throw new IllegalArgumentException("Invalid Coordinate: There is no ship contains this coordinate.");

        Placement destPlacement = readPlacement("Where do you want to move you " + oriShip.getName() + " to?\nEnter a placement:\n");
        Ship<Character> destShip = shipCreationFns.get(oriShip.getName()).apply(destPlacement);
        theBoard.tryMoveShip(destShip, oriShip);
        out.print("Player "+name+" used a special action.\n");
        numMovesLeft -= 1;
    }

    /**
     * Print Sonar result for AI.
     *
     * @param shipCount HashMap<Ship type name, number of square this type of ship occupied>
     */
    @Override
    public void printSonarResult(HashMap<String, Integer> shipCount) {
        out.print("Player "+name+" used a special action.\n");
    }

    /**
     * Try to fire at specified coordinate(read from input source) on enemy board.
     *
     * @param enemyBoard is the target board we tried to fire at.
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if the number of sonar scan is less than 1 or
     *                                  the coordinate of sonar center is out of bound
     */
    @Override
    public void tryFire(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
        Coordinate hitAt = readCoordinate("Where do you want to hit your enemy? Enter a coordinate:\n");
        Ship<Character> shipHitAt = enemyBoard.fireAt(hitAt);
        if (shipHitAt == null) {
            out.print("Player"+name+" missed at "+hitAt.toString()+"!\n");
        } else {
            out.print("Player"+name+" hit you " + shipHitAt.getName() + " at "+hitAt.toString()+"!\n");
        }
    }
}
