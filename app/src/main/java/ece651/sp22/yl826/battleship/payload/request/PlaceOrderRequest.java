package ece651.sp22.yl826.battleship.payload.request;

import ece651.sp22.yl826.battleship.order.Order;

import java.util.ArrayList;

public class PlaceOrderRequest {
    private Long roomID;
    private ArrayList<PlaceOrder> orders;

    public ArrayList<PlaceOrder> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<PlaceOrder> orders) {
        this.orders = orders;
    }

    public Long getRoomID() {
        return roomID;
    }

    public void setRoomID(Long roomID) {
        this.roomID = roomID;
    }


}
