package ece651.sp22.yl826.battleship.checker;

import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.territory.Territory;

import java.util.ArrayList;
import java.util.Map;

public class PlaceTerrIDChecker extends PlaceRuleChecker{

    /**
     * check if the territory id is same as the player id who issued this order
     * @param riskMap
     * @param unitPlaceOrders
     * @param userId
     */
    @Override
    protected void checkMyRule(RISKMap riskMap, Map<String, Integer> unitPlaceOrders, Long userId) {
        ArrayList<Territory> myTerrs = (ArrayList<Territory>) riskMap.getTerritoriesByOwnerID(userId);
        for (String terrName : unitPlaceOrders.keySet()){
            Territory t = riskMap.getTerritoryByName(terrName);
            if (!myTerrs.contains(t)){
                throw new IllegalArgumentException("You must place units in your own territories!");
            }
        }
    }
    public PlaceTerrIDChecker(PlaceRuleChecker next) {
        super(next);
    }
}
