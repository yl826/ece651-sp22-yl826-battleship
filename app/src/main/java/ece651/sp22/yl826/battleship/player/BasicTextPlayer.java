package ece651.sp22.yl826.battleship.player;

import ece651.sp22.yl826.battleship.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * A TextPlayer represent a text based player.
 */
public abstract class BasicTextPlayer {

    public Board<Character> getTheBoard() {
        return theBoard;
    }

    public BoardTextView getView() {
        return view;
    }

    public BufferedReader getInputReader() {
        return inputReader;
    }

    public PrintStream getOut() {
        return out;
    }

    public AbstractShipFactory<Character> getShipFactory() {
        return shipFactory;
    }

    public ArrayList<String> getShipsToPlace() {
        return shipsToPlace;
    }

    public HashMap<String, Function<Placement, Ship<Character>>> getShipCreationFns() {
        return shipCreationFns;
    }

    public int getNumMovesLeft() {
        return numMovesLeft;
    }

    public void setNumMovesLeft(int numMovesLeft) {
        this.numMovesLeft = numMovesLeft;
    }

    public void setNumSonarLeft(int numSonarLeft) {
        this.numSonarLeft = numSonarLeft;
    }

    public int getNumSonarLeft() {
        return numSonarLeft;
    }

    /**
     * The name of this player.
     */
    final String name;

    /**
     * The Board to store all the information.
     */
    final Board<Character> theBoard;

    /**
     * The textView to display.
     */
    final BoardTextView view;

    /**
     * An BufferedReader to read all the command.
     */
    final BufferedReader inputReader;

    /**
     * An PrintStream to print all the result.
     */
    final PrintStream out;


    /**
     * The character shipFactory to create all kind of ships.
     */
    final AbstractShipFactory<Character> shipFactory;

    /**
     * The type of ships need to place.
     */
    final ArrayList<String> shipsToPlace;

    /**
     * shipCreationFns Map.
     */
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;


    /**
     * number of Moves
     */
    int numMovesLeft;


    /**
     * number of Sonars
     */
    int numSonarLeft;


    /**
     * Constructs a TextPlayer with the specified information show as below.
     *
     * @param name            is the name of this player.
     * @param theBoard        is the board of this player.
     * @param inputReader     is the input stream of this player.
     * @param out             is the output stream of this player.
     * @param shipFactory     is the ship factory used by this player.
     * @param shipsToPlace    is the type of ships need to place.
     * @param shipCreationFns is the map between type of ship and create function.
     */
    public BasicTextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory, Iterable<String> shipsToPlace, Map<String, Function<Placement, Ship<Character>>> shipCreationFns, int numMovesLeft, int numSonarLeft) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<>();
        for (String singleShipToPlace : shipsToPlace) {
            this.shipsToPlace.add(singleShipToPlace);
        }
        this.shipCreationFns = new HashMap<>();
        for (String key : shipCreationFns.keySet()) {
            this.shipCreationFns.put(key, shipCreationFns.get(key));
        }
        this.numMovesLeft = numMovesLeft;
        this.numSonarLeft = numSonarLeft;

    }

    /**
     * Read one placement command from input and new a Placement.
     *
     * @param prompt is a friendly advise for user.
     * @return The user-specific Placement for that input command.
     * @throws IOException if the command is invalid.
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.print(prompt + "\n");
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("Invalid input: Empty line");
        }
        return new Placement(s);
    }


    /**
     * Do a single Placement
     * @param shipName is the name of this ship need to placed.
     * @param createFn is the create function.
     * @throws IOException if there is an empty line.
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {


        boolean done = false;
        while (!done) {
            try {
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                String errorMessage = theBoard.tryAddShip(s);
                if (errorMessage != null) {
                    throw new IllegalArgumentException(errorMessage);
                }
                done = true;
            } catch (IllegalArgumentException e) {
                out.print(e.getMessage() + "\n");
            }
        }
        out.print(view.displayMyOwnBoard());
    }


    /**
     * Display the empty board first, then the help message, finally the placement result.
     *
     * @throws IOException When io is not valid.
     */
    public void doPlacementPhase() throws IOException {
        //display the starting (empty) board
        out.print(view.displayMyOwnBoard() + "\n");

        //print the instructions message (from the README,
        //       but also shown again near the top of this file)
        out.print("Player " + name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n");
        for (String singleShipToPlace : shipsToPlace) {

            doOnePlacement(singleShipToPlace, shipCreationFns.get(singleShipToPlace));
        }


    }


    /**
     * get the name of this player
     *
     * @return name of this player.
     */
    public String getName() {
        return name;
    }


    /*
     * Prints a message after player wins.
     */
    public abstract void declareWinner();

}
