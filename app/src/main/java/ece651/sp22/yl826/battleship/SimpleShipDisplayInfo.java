package ece651.sp22.yl826.battleship;


/**
 *
 * @param <T> The type of info to display.
 */
public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    /**
     * The info need to display in normal condition.
     */
    private final T myData;

    /**
     * The info need to display when hit.
     */
    private final T onHit;


    /**
     * Constructs a SimpleShipDisplayInfo with the specified type of myDara and onHit.
     * @param myData is the display info of normal condition.
     * @param onHit is the display info when hit.
     */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
     * Get the display info by boolean hit.
     * @param where the coordinate of this info.
     * @param hit a boolean if hit or not
     * @return The display info based on boolean hit.
     */
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        } else {
            return myData;
        }
    }
}
