package ece651.sp22.yl826.battleship.factory;

import ece651.sp22.yl826.battleship.unit.BasicUnit;
import ece651.sp22.yl826.battleship.unit.Unit;

public class V1UnitFactory implements AbstractUnitFactory{

    /**
     * create a territory of specific type and amount
     * @param amount
     * @return the created unit
     */
    @Override
    public Unit createNSoldiers(int amount) {
        return new BasicUnit("Soldier", amount);
    }
    
}
