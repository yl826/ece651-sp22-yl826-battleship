package ece651.sp22.yl826.battleship.payload.response;

public class PlaceShipResponse {
    private String prompt;

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public PlaceShipResponse(String prompt) {
        this.prompt = prompt;
    }
}
