package ece651.sp22.yl826.battleship.payload.response;

import ece651.sp22.yl826.battleship.BattleShipBoard;
import ece651.sp22.yl826.battleship.Board;
import ece651.sp22.yl826.battleship.territory.Color;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

public class GameStatusResponse {

    private Character[][] myBoard;
    private Character[][] enemyBoard;
    private int numMoveLeft;
    private int numSonarLeft;

    public int getNumMoveLeft() {
        return numMoveLeft;
    }

    public void setNumMoveLeft(int numMoveLeft) {
        this.numMoveLeft = numMoveLeft;
    }

    public int getNumSonarLeft() {
        return numSonarLeft;
    }

    public void setNumSonarLeft(int numSonarLeft) {
        this.numSonarLeft = numSonarLeft;
    }

    public Character[][] getMyBoard() {
        return myBoard;
    }

    public void setMyBoard(Character[][] myBoard) {
        this.myBoard = myBoard;
    }

    public Character[][] getEnemyBoard() {
        return enemyBoard;
    }

    public void setEnemyBoard(Character[][] enemyBoard) {
        this.enemyBoard = enemyBoard;
    }


    private ArrayList<String> shipToPlace;

    public ArrayList<String> getShipToPlace() {
        return shipToPlace;
    }

    public void setShipToPlace(ArrayList<String> shipToPlace) {
        this.shipToPlace = shipToPlace;
    }


    private String state;

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }


    private Long winner;



    private TreeMap<Long, Color> idToColor;
    private Set<Long> lostPlayers;
    private String prompt;

    /**
     * overloaded constructor with all attributes
     *
     * @param state
     * @param battleShipBoard
     * @param winner
     * @param idToColor
     * @param prompt
     */
    public GameStatusResponse(String state, Long winner, TreeMap<Long, Color> idToColor,
                              String prompt, ArrayList<String> shipToPlace, Character[][] myBoard, Character[][] enemyBoard, int moveLeft, int sonarLeft) {
        this.state = state;
        this.winner = winner;
        this.idToColor = idToColor;
        this.prompt = prompt;
        this.shipToPlace = shipToPlace;
        this.myBoard = myBoard;
        this.enemyBoard = enemyBoard;
        this.numMoveLeft = moveLeft;
        this.numSonarLeft = sonarLeft;
    }

    /**
     * default constructor
     */
    public GameStatusResponse() {
    }

    /**
     * overloaded constructor with only prompt
     *
     * @param prompt
     */
    public GameStatusResponse(String prompt) {
        this.prompt = prompt;
    }

    /**
     * get state
     *
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * set state
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }


    /**
     * get winner
     *
     * @return winner
     */
    public Long getWinner() {
        return winner;
    }

    /**
     * set winner
     *
     * @param winner
     */
    public void setWinner(Long winner) {
        this.winner = winner;
    }

    /**
     * get id to color
     *
     * @return id to color
     */
    public TreeMap<Long, Color> getIdToColor() {
        return idToColor;
    }

    /**
     * set id to color
     *
     * @param idToColor
     */
    public void setIdToColor(TreeMap<Long, Color> idToColor) {
        this.idToColor = idToColor;
    }

    /**
     * get all lost players
     *
     * @return all lost players
     */
    public Set<Long> getLostPlayers() {
        return lostPlayers;
    }

    /**
     * set all lost players
     *
     * @param lostPlayers
     */
    public void setLostPlayers(Set<Long> lostPlayers) {
        this.lostPlayers = lostPlayers;
    }
}
