package ece651.sp22.yl826.battleship.payload.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class CreateRoomRequest {
    @NotNull
    @Min(2)
    @Max(5)
    private int roomSize;

    /**
     * get room size
     * @return room size
     */
    public int getRoomSize() {
        return roomSize;
    }

    /**
     * set room size
     * @param roomSize
     */
    public void setRoomSize(int roomSize) {
        this.roomSize = roomSize;
    }
}