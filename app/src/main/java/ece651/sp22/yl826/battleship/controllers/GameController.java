package ece651.sp22.yl826.battleship.controllers;

import ece651.sp22.yl826.battleship.APIGameHandler;
import ece651.sp22.yl826.battleship.Coordinate;
import ece651.sp22.yl826.battleship.Placement;
import ece651.sp22.yl826.battleship.Ship;
import ece651.sp22.yl826.battleship.models.State;
import ece651.sp22.yl826.battleship.payload.request.*;
import ece651.sp22.yl826.battleship.payload.response.*;
import ece651.sp22.yl826.battleship.security.services.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/game")
public class GameController {


    private HashMap<Long, APIGameHandler> rooms;

    public GameController() {
        rooms = new HashMap<>();
    }

    private long roomIDCounter;


    /**
     * This method handle the post request of /createRoom.
     *
     * @param createRoomRequest A deserialize json object contains roomSize.
     * @return ResponseEntity which contains the http code to indicate the result.
     */
    @PostMapping("/createRoom")
//    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<CreateRoomResponse> createRoom(@Valid @RequestBody CreateRoomRequest createRoomRequest) {

        Long userId = getUserId();
        System.out.println(userId);

        int roomSize = 2;
//        if (roomSize<2 || roomSize > 5) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CreateRoomResponse("Failed to create a room! Room size must be between 2~5!", null));
//        }
        APIGameHandler gameHandler = new APIGameHandler(roomSize, roomIDCounter++, userId);

        rooms.put(gameHandler.getRoomID(), gameHandler);
        return ResponseEntity.ok(new CreateRoomResponse("Successfully create a game room!", gameHandler.getRoomID()));

    }


    /**
     * This method handle the post request of /joinRoom.
     *
     * @param joinRoomRequest A deserialize json object contains roomID.
     * @return ResponseEntity which contains the http code to indicate the result.
     */
    @PostMapping("/joinRoom")
//    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<JoinRoomResponse> joinRoom(@Valid @RequestBody JoinRoomRequest joinRoomRequest) {

        Long userId = getUserId();
        Long roomID = joinRoomRequest.getRoomID();
        APIGameHandler apiGameHandler = rooms.get(roomID);

        if (apiGameHandler == null || !apiGameHandler.tryAddPlayer(userId)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JoinRoomResponse("Failed to join! Room not found or Room full!", roomID));
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(new JoinRoomResponse("Successfully joined a game room!", roomID));
        }


    }

    /**
     * This is a helper function to get the current user id.
     *
     * @return The user ID of current user.
     */
    protected Long getUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return userDetails.getId();
    }


    /**
     * This method handle the get request of /gameStatus.
     *
     * @param roomID The roomID to look up.
     * @return ResponseEntity which contains the http code to indicate the result and the room status if the roomID is valid.
     */
    @GetMapping("/gameStatus")
    public ResponseEntity<GameStatusResponse> gameStatus(@RequestParam Long roomID) {
        Long userId = getUserId();

        if (rooms.containsKey(roomID) && rooms.get(roomID).getPlayers().contains(userId)) {
            APIGameHandler apiGameHandler = rooms.get(roomID);

            long enemy_user_id = userId;
            for (Long id: rooms.get(roomID).getPlayers()){
                if(id != userId)
                    enemy_user_id = id;
            }

            System.out.println(userId);
            System.out.println(enemy_user_id);


            return ResponseEntity.status(HttpStatus.OK).body(new GameStatusResponse(
                    apiGameHandler.getPlayerState(userId),
                    apiGameHandler.getWinner(),
                    apiGameHandler.getIdToColor(),
                    "",
                    apiGameHandler.getBattleshipPlayers().get(userId).getShipsToPlace(),
                    apiGameHandler.getBattleshipPlayers().get(userId).getView().generateMyOwnBoard(),
                    apiGameHandler.getBattleshipPlayers().get(enemy_user_id).getView().generateEnemyBoard(),
                    apiGameHandler.getBattleshipPlayers().get(userId).getNumMovesLeft(),
                    apiGameHandler.getBattleshipPlayers().get(userId).getNumSonarLeft()

            ));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GameStatusResponse("Room not found!"));
        }
    }




    /**
     * This method handle the post request of /place/ship.
     *
     * @param PlaceShipRequest A deserialize json object contains some Unit object.
     * @return ResponseEntity which contains the http code to indicate the result.
     */
    @PostMapping("/place/ship")
    public ResponseEntity<PlaceShipResponse> placeUnit(@Valid @RequestBody PlaceShipRequest placeShipRequest) {
        Long userId = getUserId();
        Long roomID = placeShipRequest.getRoomID();
        APIGameHandler currGame = rooms.get(roomID);

        if (currGame == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new PlaceShipResponse("Cannot find room " + roomID + "!"));
        }
        String place_error_message = "";
        for(PlaceShip ship: placeShipRequest.getShipPlaceOrders()){
            System.out.println(ship.getShipName());
            System.out.println(currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns().get(ship.getShipName()));
            System.out.println(currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns());
            try{
                Placement placement = new Placement(new Coordinate(ship.getRow()+ship.getColumn()),ship.getOrientation());
                Ship<Character> s = currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns().get(ship.getShipName()).apply(placement);
                String error_message = currGame.getBattleshipPlayers().get(getUserId()).getTheBoard().tryAddShip(s);
                if(error_message != null)
                    place_error_message += error_message;
            }catch (Exception e){
                place_error_message += e.toString();
            }
        }


        //String place_error_message = currGame.tryPlaceUnit(userId, placeUnitRequest.getUnitPlaceOrders());

        if (!place_error_message.equals("")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new PlaceShipResponse(place_error_message));
        } else {
            currGame.addCommittedPlayer(getUserId());
            return ResponseEntity.status(HttpStatus.OK).body(new PlaceShipResponse("Successfully placed ship into board."));

        }

    }

    /**
     * This method handle the post request of /place/order.
     *
     * @param placeOrderRequest A deserialize json object contains some Order object.
     * @return ResponseEntity which contains the http code to indicate the result.
     */
    @PostMapping("/place/order")
    public ResponseEntity<PlaceUnitResponse> placeOrder(@Valid @RequestBody PlaceOrderRequest placeOrderRequest) {
        Long userId = getUserId();
        Long roomID = placeOrderRequest.getRoomID();

        APIGameHandler currGame = rooms.get(roomID);
        long enemy_user_id = userId;
        for (Long id: rooms.get(roomID).getPlayers()){
            if(id != userId)
                enemy_user_id = id;
        }
        String sonar_result = "";
        String order_error_message = "";
        for(PlaceOrder order: placeOrderRequest.getOrders()){
//            System.out.println(order.getShipName());
//            System.out.println(currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns().get(ship.getShipName()));
//            System.out.println(currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns());
            try{
                switch (order.getOrderType()){
                    case "Move":

                        if(currGame.getBattleshipPlayers().get(userId).getNumMovesLeft() < 1){
                            throw new IllegalArgumentException("Invalid Move Action: Already used up!");
                        }
                        Coordinate oriCoordinate = new Coordinate(order.getSource_row()+order.getSource_col());

                        Ship<Character> oriShip = currGame.getBattleshipPlayers().get(getUserId()).getTheBoard().getShipAt(oriCoordinate);
                        if (oriShip == null)
                            throw new IllegalArgumentException("Invalid Coordinate: There is no ship contains this coordinate.");

                        Placement destPlacement = new Placement(new Coordinate(order.getRow()+order.getCol()), order.getOri().charAt(0));
                        Ship<Character> destShip = currGame.getBattleshipPlayers().get(getUserId()).getShipCreationFns().get(oriShip.getName()).apply(destPlacement);
                        currGame.getBattleshipPlayers().get(getUserId()).getTheBoard().tryMoveShip(destShip, oriShip);
                        currGame.getBattleshipPlayers().get(userId).setNumMovesLeft(currGame.getBattleshipPlayers().get(userId).getNumMovesLeft()-1);
                        break;
                    case "Fire":


                        Coordinate hitAt = new Coordinate(order.getRow()+order.getCol());

                        Ship<Character> shipHitAt = currGame.getBattleshipPlayers().get(enemy_user_id).getTheBoard().fireAt(hitAt);
                        break;
                    case "Sonar Scan":

                        if(currGame.getBattleshipPlayers().get(userId).getNumSonarLeft() < 1){
                            throw new IllegalArgumentException("Invalid Sonar Scan Action: Already used up!");
                        }
                        Coordinate sonarAt = new Coordinate(order.getRow()+order.getCol());
                        int sonarRadius = 3;
                        HashMap<String, Integer> shipCount = currGame.getBattleshipPlayers().get(enemy_user_id).getTheBoard().sonar(sonarRadius, sonarAt);
                        sonar_result = currGame.getBattleshipPlayers().get(userId).generateSonarResult(shipCount);
                        currGame.getBattleshipPlayers().get(userId).setNumMovesLeft(currGame.getBattleshipPlayers().get(userId).getNumSonarLeft()-1);
                        //printSonarResult(shipCount);
//                        numSonarLeft -= 1;
                        break;
                    default:
                        order_error_message += "Error order type!";
                }
            }catch (Exception e){
                order_error_message += e.toString();
            }
        }

        if (!order_error_message.equals("")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new PlaceUnitResponse(order_error_message));
        } else {
            currGame.addCommittedPlayer(userId);
            String res = "Successfully placed order into board!";
            if(! sonar_result.equals(""))
                res += sonar_result;
            return ResponseEntity.status(HttpStatus.OK).body(new PlaceUnitResponse(res));
        }

    }

    /**
     * This method handle the get request of /rooms/available.
     *
     * @return ResponseEntity which contains the http code to indicate the result and the available rooms.
     */
    @GetMapping("/rooms/available")
    public ResponseEntity<RoomsAvailableResponse> allRooms() {
        Long userId = getUserId();

        List<APIGameHandler> res = rooms.entrySet().stream()
                .filter(e -> (State.WaitingToStartState.name().equals(e.getValue().getCurrentState())
                        && !e.getValue().getPlayers().contains(userId)))
                .map(Map.Entry::getValue).collect(Collectors.toList());

        ArrayList<RoomsAvailable> r = new ArrayList<>();
        for (APIGameHandler apiGameHandler: res){
            r.add(new RoomsAvailable(apiGameHandler.getRoomID(),apiGameHandler.getRoomSize()));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new RoomsAvailableResponse(r));
    }


    /**
     * This method handle the get request of /rooms/joined.
     *
     * @return ResponseEntity which contains the http code to indicate the result and the joined rooms.
     */
    @GetMapping("/rooms/joined")
    public ResponseEntity<RoomsAvailableResponse> joinedRooms() {
        Long userId = getUserId();

        List<APIGameHandler> res = rooms.entrySet().stream()
                .filter(e -> e.getValue().getPlayers().contains(userId)).map(Map.Entry::getValue).collect(Collectors.toList());

        ArrayList<RoomsAvailable> r = new ArrayList<>();
        for (APIGameHandler apiGameHandler: res){
            r.add(new RoomsAvailable(apiGameHandler.getRoomID(),apiGameHandler.getRoomSize()));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new RoomsAvailableResponse(r));
    }


}