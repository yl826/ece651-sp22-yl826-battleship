package ece651.sp22.yl826.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This class store the BattleShipBoard.
 */
public class BattleShipBoard<T> implements Board<T> {



    public ArrayList<Ship<T>> getMyShips() {
        return myShips;
    }

    public T getMissInfo() {
        return missInfo;
    }

    public HashSet<Coordinate> getEnemyMisses() {
        return enemyMisses;
    }

    public void setEnemyMisses(HashSet<Coordinate> enemyMisses) {
        this.enemyMisses = enemyMisses;
    }

    public HashMap<Coordinate, T> getEnemyOnceHitButMove() {
        return enemyOnceHitButMove;
    }

    public void setEnemyOnceHitButMove(HashMap<Coordinate, T> enemyOnceHitButMove) {
        this.enemyOnceHitButMove = enemyOnceHitButMove;
    }

    public HashSet<Coordinate> getMyHitMove() {
        return myHitMove;
    }

    public void setMyHitMove(HashSet<Coordinate> myHitMove) {
        this.myHitMove = myHitMove;
    }

    /**
     * placementChecker to verify the validity of any placement.
     */
    private final PlacementRuleChecker<T> placementChecker;

    /**
     * An arrayList to stored ships.
     */
    final ArrayList<Ship<T>> myShips;

    /**
     * width for BattleShipBoard.
     */
    private final int width;

    /**
     * height for BattleShipBoard.
     */
    private final int height;

    /**
     * the info to display when miss.
     */
    final T missInfo;

    /**
     * Track where the enemy has missed
     */
    HashSet<Coordinate> enemyMisses;

    /**
     * Track the enemy hits when moving our ship
     */
    private HashMap<Coordinate, T> enemyOnceHitButMove;

    /**
     * Track my hits when moving our ship
     */
    private HashSet<Coordinate> myHitMove;


    /**
     * Constructs a BattleShipBoard with the specified width and height
     *
     * @param width  is the width of the newly constructed board.
     * @param height is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int width, int height, T missInfo) {
        this(width, height, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)), missInfo);
    }

    /**
     * Constructs a BattleShipBoard with the specified width and height
     *
     * @param width            is the width of the newly constructed board.
     * @param height           is the height of the newly constructed board.
     * @param placementChecker is the rules need to check.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int width, int height, PlacementRuleChecker<T> placementChecker, T missInfo) {
        if (width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
        }
        this.width = width;
        this.height = height;
        this.myShips = new ArrayList<>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<>();
        this.missInfo = missInfo;
        this.enemyOnceHitButMove = new HashMap<Coordinate, T>();
        this.myHitMove = new HashSet<Coordinate>();

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Try to add ship toAdd to the Board object.
     *
     * @param toAdd A ship need to add.
     * @return True if we successfully add this ship, otherwise False.
     */
    public String tryAddShip(Ship<T> toAdd) {
        String errorMessage = placementChecker.checkPlacement(toAdd, this);
        if (errorMessage == null) {
            myShips.add(toAdd);
        }

        return errorMessage;
    }

    /**
     * Return the view-specific information at the given coordinate. Null if none is fund ar the location.
     *
     * @param where is the coordinate to return information for
     * @return The view-specific information at that coordinate.
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * Return the view-specific information at the given coordinate. Null if none is fund ar the location.
     *
     * @param where is the coordinate to return information for
     * @return The view-specific information at that coordinate.
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (where.getColumn() > width - 1 || where.getRow() > height - 1) {
            throw new IllegalArgumentException("The coordinate is out of bound in board!");
        }
        if (!isSelf && enemyOnceHitButMove.get(where) != null) {
            return enemyOnceHitButMove.get(where);
        }
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                if (!isSelf && myHitMove.contains(where)) {
                    return null;
                }
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;

    }

    /**
     * Search for any ship that occupies coordinate c
     * If one is found, that Ship is "hit" by the attack and should record it.
     *
     * @param c the coordinate of fire
     * @return The ship fire at or null if no ship at c.
     */
    public Ship<T> fireAt(Coordinate c) {
        String errorMessage = placementChecker.checkPlacement(c, this);
        if (errorMessage != null) {
            throw new IllegalArgumentException(errorMessage);
        }
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(c)) {
                s.recordHitAt(c);
                return s;
            }
        }
        enemyMisses.add(c);
        return null;

    }

    /**
     * Return the view-specific information at the given coordinate. Null if none is fund ar the location.
     *
     * @param where is the coordinate to return information for
     * @return The view-specific information at that coordinate.
     */
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    /**
     * Check if all of player's is sunk. Which means there is a winner.
     *
     * @return True if all the ship player has is sunk, false otherwise.
     */
    public boolean isAllSunk() {
        for (Ship<T> ship : myShips) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;

    }

    /**
     * Get ship at specified coordinate
     *
     * @param where the coordinate to check
     * @return ship at where
     */
    public Ship<T> getShipAt(Coordinate where) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Try to move a ship.
     *
     * @param destShip   the ship to add
     */
    public void tryMoveShip(Ship<T> destShip, Ship<T> oriShip) throws IllegalArgumentException {

        String errorMessage = placementChecker.checkPlacement(oriShip,destShip, this);
        if (errorMessage == null) {
            myShips.remove(oriShip);
            myShips.add(destShip);
        }
        else {
            throw new IllegalArgumentException(errorMessage);
        }

        for (Coordinate piece : oriShip.getCoordinates()) {
            //if(whatIsAt(piece,true)==hitInfo){
            if (oriShip.wasHitAt(piece)) {
                enemyOnceHitButMove.put(piece, oriShip.getDisplayInfoAt(piece, false));
                //Coordinate newHitLoc = oriShip.getRelativeCoordinate(piece, destShip.getUpperLeft());
                int tmpIndex = oriShip.getRelativeIndex(piece);
                Coordinate newHitLoc = destShip.getCoordinateByRelativeIndex(tmpIndex);
                fireAt(newHitLoc);

                myHitMove.add(newHitLoc);
            }
        }


    }


    /**
     * Sonar scan: returns number of squares occupied by each ship present around a
     * coordinate C in the following pattern:
     * *
     * ***
     * *****
     * ***C***
     * *****
     * ***
     * *
     *
     * @param where is the center coordinate C around which to find ships
     * @throws IllegalArgumentException if C is not part of the battleship
     * @returns a hashmap of ship name and the number of squares occupied by it.
     */
    public HashMap<String, Integer> sonar(int range, Coordinate where) throws IllegalArgumentException {

        String errorMessage = placementChecker.checkPlacement(where, this);
        if (errorMessage != null) {
            throw new IllegalArgumentException(errorMessage);
        }
        // create coordinates in the range of sonar
        ArrayList<Coordinate> sonarCoords = new ArrayList<Coordinate>();
        int columnRange;
        for (int row = -range; row <= range; row++) {
            columnRange = range - Math.abs(row);
            for (int col = Math.min(columnRange, -columnRange); col <= Math.max(columnRange, -columnRange); col++) {
                sonarCoords.add(new Coordinate(where.getRow() + row, where.getColumn() + col));
            }
        }

        // count ships present at sonarCoords
        HashMap<String, Integer> shipCount = new HashMap<String, Integer>();
        for (Ship<T> ship : myShips) {
            shipCount.put(ship.getName(), 0);
        }

        int count;
        // Iterable<Coordinate> shipCoords = new Iterable<Coordinate>();
        for (Ship<T> ship : myShips) {
            for (Coordinate c : sonarCoords) {
                Iterable<Coordinate> shipCoords = ship.getCoordinates();
                for (Coordinate shipCoord : shipCoords) {
                    if (c.equals(shipCoord)) {
                        count = shipCount.get(ship.getName());
                        shipCount.put(ship.getName(), count + 1);
                    }
                }
            }
        }
        return shipCount;
    }


}
