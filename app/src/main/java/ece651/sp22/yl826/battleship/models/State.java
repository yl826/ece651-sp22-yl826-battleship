package ece651.sp22.yl826.battleship.models;


public enum State {
    WaitingState,
    PlacingState,
    OrderingState,
    LostState,
    EndState,
    WaitingToStartState


}