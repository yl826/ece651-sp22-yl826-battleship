package ece651.sp22.yl826.battleship.payload.response;

import ece651.sp22.yl826.battleship.APIGameHandler;

import java.util.List;

public class RoomsAvailableResponse {
    private List<RoomsAvailable> rooms;

    public RoomsAvailableResponse(List<RoomsAvailable> rooms) {
        this.rooms = rooms;
    }

    public List<RoomsAvailable> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomsAvailable> rooms) {
        this.rooms = rooms;
    }
}
