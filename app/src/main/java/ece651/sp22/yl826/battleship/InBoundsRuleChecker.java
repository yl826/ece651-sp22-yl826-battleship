package ece651.sp22.yl826.battleship;

/**
 * Check the validity of any placement by the bound of board.
 *
 * @param <T> The type of ship and Board.
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * Check if the dest ship is valid.
     *
     * @param oriShip is the ori ship.
     * @param destShip is the dest ship we need to check.
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    @Override
    protected String checkMyRule(Ship<T> oriShip, Ship<T> destShip, Board<T> theBoard) {
        return checkMyRule(destShip, theBoard);
    }

    /**
     * Check if theShip is valid.
     *
     * @param theShip  is the ship need to check.
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            String errorMseeage = this.checkMyRule(c,theBoard);
            if(errorMseeage != null)
                return errorMseeage;


        }
        return null;
    }

    /**
     * Check if coordinate is valid.
     *
     * @param theCoordinate is coordinate we need to check if is in bound.
     * @param theBoard      id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    @Override
    protected String checkMyRule(Coordinate theCoordinate, Board<T> theBoard) {


        if (theCoordinate.getRow() < 0) {
            return "That placement is invalid: the ship goes off the top of the board.";
        }
        if (theCoordinate.getRow() >= theBoard.getHeight()) {
            return "That placement is invalid: the ship goes off the bottom of the board.";
        }
        if (theCoordinate.getColumn() < 0) {
            return "That placement is invalid: the ship goes off the left of the board.";
        }
        if (theCoordinate.getColumn() >= theBoard.getWidth()) {
            return "That placement is invalid: the ship goes off the right of the board.";
        }
        return null;
    }

    /**
     * Construct a PlacementRuleChecker with specified next.
     *
     * @param next is the next PlacementRuleChecker need to check.
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
