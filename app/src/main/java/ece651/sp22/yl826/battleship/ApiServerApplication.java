package ece651.sp22.yl826.battleship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiServerApplication {

    /**
     * program entry point
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ApiServerApplication.class, args);
    }

}
