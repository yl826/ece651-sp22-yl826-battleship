package ece651.sp22.yl826.battleship.repository;


import ece651.sp22.yl826.battleship.models.ERole;
import ece651.sp22.yl826.battleship.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}