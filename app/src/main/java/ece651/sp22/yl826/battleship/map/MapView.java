package ece651.sp22.yl826.battleship.map;

public interface MapView {
  /**
   * Display the map at initial phase
   * @return map information in string
   */
  public String displayMapInit();

  /**
   * Display the map
   * @return map information in string
   */
  public String displayMap();
}
