package ece651.sp22.yl826.battleship;

import ece651.sp22.yl826.battleship.factory.RandomMapFactory;
import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.models.State;
import ece651.sp22.yl826.battleship.order.Order;
import ece651.sp22.yl826.battleship.player.TextPlayer;
import ece651.sp22.yl826.battleship.territory.Color;
import ece651.sp22.yl826.battleship.territory.Owner;
import ece651.sp22.yl826.battleship.territory.Territory;
import ece651.sp22.yl826.battleship.unit.BasicUnit;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class APIGameHandler {


    private Long winner;

    public Long getWinner() {
        return winner;
    }

    private HashMap<Long, TextPlayer> battleshipPlayers;

    public HashMap<Long, TextPlayer> getBattleshipPlayers() {
        return battleshipPlayers;
    }

    public void setBattleshipPlayers(HashMap<Long, TextPlayer> battleshipPlayers) {
        this.battleshipPlayers = battleshipPlayers;
    }

    /////////////////////////////////////////Add battleship related field above
    //logger to display info in server console
    Logger logger;

    //RISK game related fields
    private ArrayList<Color> predefineColorList = new ArrayList<>();
    private Set<Long> players; //all joined players
    private TreeMap<Long, Color> idToColor; //player id to color

    private String currentState; //game's current state
    private Set<Long> commitedPlayer; //all committed players

    public void setLostPlayer(Set<Long> lostPlayer) {
        this.lostPlayer = lostPlayer;
    }

    private Set<Long> lostPlayer; //all losted players
    private RISKMap riskMap; //the map to play with
    private Integer InitUnitAmountPerPlayer; //Initial Total Unit amount available for each player
    private ArrayList<Order> temporaryOrders; // temporary order holder

    //room related fields
    private final int roomSize;
    private final long roomID;

    //getters/setters
    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public ArrayList<Color> getPredefineColorList() {
        return predefineColorList;
    }

    public void setPredefineColorList(ArrayList<Color> predefineColorList) {
        this.predefineColorList = predefineColorList;
    }

    public Set<Long> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Long> players) {
        this.players = players;
    }

    public TreeMap<Long, Color> getIdToColor() {
        return idToColor;
    }

    public void setIdToColor(TreeMap<Long, Color> idToColor) {
        this.idToColor = idToColor;
    }

    public RISKMap getRiskMap() {
        return riskMap;
    }

    public void setRiskMap(RISKMap riskMap) {
        this.riskMap = riskMap;
    }

    public int getRoomSize() {
        return roomSize;
    }

    public long getRoomID() {
        return roomID;
    }


    //constructor

    /**
     * create a new room
     *
     * @param roomSize
     * @param roomID
     * @param hostID
     */
    public APIGameHandler(int roomSize, long roomID, Long hostID) {
        this.roomSize = roomSize;
        this.roomID = roomID;
        predefineColorList.add(new Color("Red"));
        predefineColorList.add(new Color("Green"));
        predefineColorList.add(new Color("Blue"));
        predefineColorList.add(new Color("Yellow"));
        predefineColorList.add(new Color("Purple"));
        idToColor = new TreeMap<Long, Color>();
        riskMap = (RISKMap) new RandomMapFactory().createMapForNplayers(roomSize);
        players = new HashSet<>();
        players.add(hostID);
        commitedPlayer = new HashSet<>();
        temporaryOrders = new ArrayList<>();
        logger = LoggerFactory.getLogger(APIGameHandler.class);
        this.currentState = State.WaitingToStartState.name();
        InitUnitAmountPerPlayer = 30;
        lostPlayer = new HashSet<>();

/////////////////////////////init battleship related field
        battleshipPlayers = new HashMap<>();
        battleshipPlayers.put(hostID, new TextPlayer("A", new BattleShipBoard<>(10, 20, 'X'),
                null, System.out, new V2ShipFactory(), 3, 3));
        winner = null;
    }



    /**
     * return the riskMap according to player state
     *
     * @return RISKMap
     */
    public Board<Character> getBattleshipBoardByUser(Long userId) {
        return battleshipPlayers.get(userId).getTheBoard();
    }

    /**
     * try to add a player into this game
     *
     * @param clientID
     * @return
     */
    public boolean tryAddPlayer(Long clientID) {
        //check if all players joined, check if this player has joined already
        if (players.size() == roomSize || players.contains(clientID)) {
            return false;
        } else {
            players.add(clientID);
            battleshipPlayers.put(clientID, new TextPlayer("A", new BattleShipBoard<>(10, 20, 'X'),
                    null, System.out, new V2ShipFactory(), 3, 3));
            //if have all players, start the game
            if (players.size() == roomSize) {
                unitPlacementPhase(3);
            }
            return true;
        }
    }


    /**
     * Record the client who successfully committed the order to make sure that the game plays in turns!
     * @param clientID The player's id which need to be record.
     */
    public void addCommittedPlayer(Long clientID) {
        commitedPlayer.add(clientID);

        Long enemy_id = clientID;
        for (Long id : players) {
            if (clientID != id) {
                enemy_id = id;
            }
        }
        if ( currentState.equals(State.OrderingState.name()) && battleshipPlayers.get(enemy_id).checkLose()) {
            winner = clientID;
            showGameResultPhase();
        }

        if (commitedPlayer.size() == roomSize) {
                orderingPhase();
        }
    }








    /**
     * assign a color to each player
     */
    public void assignColorToPlayers() {
        for (Long clientID : players) {
            idToColor.put(clientID, predefineColorList.remove(0));
        }
    }

    /**
     * Randomly initialize the territories with client ID.
     *
     * @param n_Terr_per_player
     */
    public void assignTerritoriesToPlayers(int n_Terr_per_player) {
        ArrayList<Territory> randomized = new ArrayList<>();
        for (Territory territory : riskMap.getContinent()) {
            randomized.add(territory);
        }
        Collections.shuffle(randomized, new Random(1777));
        int count = 0;
        ArrayList<Long> clientIDList = new ArrayList<>();
        for (Long clientID : players) {
            clientIDList.add(clientID);
            riskMap.tryAddOwner(new Owner(clientID));
        }
        for (Territory territory : randomized) {
            territory.tryChangeOwnerTo(clientIDList.get(count++ / n_Terr_per_player));
        }
        for (Long clientID : clientIDList) {
            int totalRest = 10;
            Random random = new Random();
            while (totalRest > 0) {
                for (Territory t : riskMap.getTerritoriesByOwnerID(clientID)) {
                    if (random.nextBoolean()) {
                        t.increaseSize(2);
                        totalRest -= 2;
                    }
                }
            }
        }
    }

    /**
     * get the state of some player
     *
     * @param clientID
     * @return
     */
    public String getPlayerState(Long clientID) {
        //check if this player lost
        if (isPlayerLost(clientID))
            return State.LostState.name();
        else {
            //check if this player has committed
            if (commitedPlayer.contains(clientID))
                return State.WaitingState.name();
            else
                return currentState;
        }
    }

    /**
     * going into the unitplacement phase after all players joined the room
     *
     * @param n_Terr_per_player
     */
    public void unitPlacementPhase(int n_Terr_per_player) {
        currentState = State.PlacingState.name();
        commitedPlayer.clear();

        assignColorToPlayers();
        assignTerritoriesToPlayers(n_Terr_per_player);
    }

    /**
     * going into placing order for each player
     */
    public void orderingPhase() {
        for (Long id : players) {
            isPlayerLost(id);
        }
        currentState = State.OrderingState.name();
        commitedPlayer.clear();
        commitedPlayer.addAll(lostPlayer);
        temporaryOrders.clear();
    }

    /**
     * show the game result if this game has a winner
     */
    public void showGameResultPhase() {
        currentState = State.EndState.name();
        commitedPlayer.clear();
    }




    /**
     * Check if any player wins.
     *
     * @return Client if this player is the only player left, otherwise null.
     */
    public Long checkWinner() {
        HashSet<Long> IDset = new HashSet<>();
        for (Territory t : riskMap.getContinent()) {
            IDset.add(t.getOwnerID());
        }

        logger.info(String.format("RoomID:%d Current IDset.size()=%d", roomID, IDset.size()));
        if (IDset.size() == 1) {
            return IDset.iterator().next();
        }
        return null;
    }


    /**
     * Check if a player is lost.
     *
     * @param clientID The player we want to check with.
     * @return True if this player is lost, otherwise false.
     */
    public boolean isPlayerLost(Long clientID) {
        //If this player still have any territory,
        //means that this player is not lost.
        if (lostPlayer.contains(clientID)) {
            return true;
        }
        if (currentState.equals(State.WaitingToStartState.name())) {
            return false;
        }

        for (Territory territory : riskMap.getContinent()) {
            if (territory.getOwnerID().equals(clientID))
                return false;
        }
        lostPlayer.add(clientID);
        return true;
    }
}
