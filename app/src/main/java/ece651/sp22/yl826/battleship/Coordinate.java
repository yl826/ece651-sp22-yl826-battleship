package ece651.sp22.yl826.battleship;

/**
 * This class store the coordinate.
 */
public class Coordinate {

    /**
     * row for the Coordinate.
     */
    private final int row;

    /**
     * column for the Coordinate.
     */
    private final int column;

    /**
     * Constructs a Coordinate with the specified row and column.
     * @param row is the row of the newly constructed coordinate.
     * @param column is the of column of the newly constructed coordinate.
     */
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;

    }

    /**
     * Constructs a Coordinate with the specified row and column(Using String format such as "A5" for row(0), column(5).
     * @param s is the string format of the row and column. Such "A2" makes the Coordinate
     * that corresponds to that string (e.g. row=0, column =2).
     * @throws IllegalArgumentException if the param s is not valid for converting to number.
     */
    public Coordinate(String s) {
        s = s.toUpperCase();
        if (s.length() != 2) {
            throw new IllegalArgumentException("Invalid string length for row and column: " + s);
        }
        String rowString = s.substring(0, 1);
        String columnString = s.substring(1);
        if (rowString.charAt(0) < 'A' || rowString.charAt(0) > 'Z') {
            throw new IllegalArgumentException("Invalid char for row: " + rowString);
        }
        this.row = rowString.charAt(0) - 'A';

        if (columnString.charAt(0) < '0' || columnString.charAt(0) > '9') {
            throw new IllegalArgumentException("Invalid string for column: " + columnString);
        }

        this.column = columnString.charAt(0) - '0';
    }

    public int getRow() {
        return row;
    }


    public int getColumn() {
        return column;
    }

    /**
     *
     * @param o Another Object need to compare with.
     * @return true if this is numerically equal to the other.
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    /**
     *
     * @return Using the String's hashCode method to generate hashCode.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     *
     * @return A String in the format of "(row, column)".
     */
    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }


}
