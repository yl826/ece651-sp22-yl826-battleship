package ece651.sp22.yl826.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board
 * (i.e., converting it to a string to show to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Construct a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display.
     * @throws IllegalArgumentException if the board is larger than 10*26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException("Board must be no larger than 10*26, but is " + toDisplay.getWidth() + "*" + toDisplay.getHeight());
        }
    }


    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board from itself's view
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board from itself's view
     */
    public Character[][] generateMyOwnBoard() {
        return generateAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board from enemy's view
     */
    public String displayEnemyBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board from enemy's view
     */
    public Character[][] generateEnemyBoard() {
        return generateAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder();
        ans.append(this.makeHeader());
        char row_alph = 'A';

        for (int i = 0; i < toDisplay.getHeight(); i++) {
            ans.append(row_alph);
            ans.append(" ");

            String sep = "";
            StringBuilder blankBoardRow = new StringBuilder();
            for (int j = 0; j < toDisplay.getWidth(); j++) {
                blankBoardRow.append(sep);
                //blankBoardRow.append(toDisplay.whatIsAt(new Coordinate(j,i))==null?" ":"s");
                if (getSquareFn.apply(new Coordinate(i, j)) == null) {
                    blankBoardRow.append(" ");
                } else {
                    blankBoardRow.append(getSquareFn.apply(new Coordinate(i, j)));
                }
                sep = "|";
            }
            ans.append(blankBoardRow);
            ans.append(" ");
            ans.append(row_alph);
            row_alph += 1;
            ans.append("\n");
        }
        ans.append(this.makeHeader());
        return ans.toString();
    }

    /**
     * This makes the multi body line, e.g. A  | |  A\n
     *
     * @return the String that is the body lines for the given board
     */
    public Character[][] generateAnyBoard(Function<Coordinate, Character> getSquareFn) {
//        StringBuilder ans = new StringBuilder();
//        ans.append(this.makeHeader());
        Character[][] res = new Character[toDisplay.getHeight()][toDisplay.getWidth()];
        //char row_alph = 'A';

        for (int i = 0; i < toDisplay.getHeight(); i++) {
            //ans.append(row_alph);
            //ans.append(" ");

            //String sep = "";
            //StringBuilder blankBoardRow = new StringBuilder();
            for (int j = 0; j < toDisplay.getWidth(); j++) {
                //blankBoardRow.append(sep);
                //blankBoardRow.append(toDisplay.whatIsAt(new Coordinate(j,i))==null?" ":"s");
                if (getSquareFn.apply(new Coordinate(i, j)) == null) {
                    //blankBoardRow.append(" ");
                    res[i][j] = ' ';
                } else {
//                    blankBoardRow.append(getSquareFn.apply(new Coordinate(i, j)));
                    res[i][j] = getSquareFn.apply(new Coordinate(i, j));
                }
//                sep = "|";
            }
//            ans.append(blankBoardRow);
//            ans.append(" ");
//            ans.append(row_alph);
//            row_alph += 1;
//            ans.append("\n");
        }
//        ans.append(this.makeHeader());
//        return ans.toString();
        return res;
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  ");
        String sep = "";
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * Display board with enemy board next to it.
     *
     * @param enemyView   is the view of enemy board
     * @param myHeader    is the header to show in my board
     * @param enemyHeader is the header to show in enemy board
     * @return is a multi line string contain my board and enemy board side by side
     **/
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        StringBuilder res = new StringBuilder();


        res.append(nSpace(5));
        res.append(myHeader);
        res.append(nSpace(2 * toDisplay.getWidth() + 22 - myHeader.length() - 5));
        res.append(enemyHeader);
        res.append("\n");

        String[] ownBoardList = displayMyOwnBoard().split("\n");
        String[] enemyBoardList = enemyView.displayEnemyBoard().split("\n");
        for (int i = 0; i < ownBoardList.length; i++) {
            res.append(ownBoardList[i]);
            res.append(nSpace(16));
            if (i == 0 || i == ownBoardList.length - 1) {
                res.append(nSpace(2));
            }
            res.append(enemyBoardList[i]);
            res.append("\n");
        }
        return res.toString();
    }

    /**
     * Calculate the space we need to build the side by side display board.
     *
     * @param width is the width of this line.
     * @return String contain width space.
     */
    private String nSpace(int width) {
        return " ".repeat(Math.max(0, width));
    }

}
