package ece651.sp22.yl826.battleship;
/**
 * Check the validity of any placement by the non-collision policy.
 * @param <T> The type of ship and Board.
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * Construct a PlacementRuleChecker with specified next.
     *
     * @param next is the next PlacementRuleChecker need to check.
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check if thsShip can be place on board without any collision to another ship.
     *
     * @param theShip  is the ship need to check.
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if(theBoard.whatIsAtForSelf(c) != null)
                return "That placement is invalid: the ship overlaps another ship.";
        }
        return null;
    }

    /**
     * Just a placeholder when checking only coordinate, Which means no collision need to check.
     *
     * @param theCoordinate is the coordinate need to check with.
     * @param theBoard      id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    @Override
    protected String checkMyRule(Coordinate theCoordinate, Board<T> theBoard) {
        return null;
    }

    /**
     * Check if destShip can be place on board without any collision to another ship(Except the oriShip).
     * @param oriShip the original ship that need to be moved
     * @param destShip the destination ship that need to be placed
     * @param theBoard the board where this movement happened.
     * @return null if no invalid situation, otherwise a String describe the error.
     */
    protected String checkMyRule(Ship<T> oriShip,Ship<T> destShip, Board<T> theBoard) {
        for (Coordinate c : destShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c) != null && !oriShip.occupiesCoordinates(c))
                return "That placement is invalid: the ship overlaps another ship.";
        }
        return null;
    }

}
