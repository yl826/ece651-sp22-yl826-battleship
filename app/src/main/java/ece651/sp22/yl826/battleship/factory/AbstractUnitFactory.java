package ece651.sp22.yl826.battleship.factory;

import ece651.sp22.yl826.battleship.unit.Unit;

/**
 * Interface for unit factory.
 */
public interface AbstractUnitFactory {
    Unit createNSoldiers(int amount);
}
