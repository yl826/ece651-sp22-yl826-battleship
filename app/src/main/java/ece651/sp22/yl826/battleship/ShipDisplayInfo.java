package ece651.sp22.yl826.battleship;

/**
 *
 * @param <T> The type of info to display.
 */
public interface ShipDisplayInfo<T> {

    /**
     * Get the display info by boolean hit.
     * @param where the coordinate of this info.
     * @param hit a boolean if hit or not
     * @return The display info based on boolean hit.
     */
    T getInfo(Coordinate where, boolean hit);
}
