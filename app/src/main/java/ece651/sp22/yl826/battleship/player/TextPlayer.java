package ece651.sp22.yl826.battleship.player;

import ece651.sp22.yl826.battleship.*;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;


/**
 * A TextPlayer represent a text based player.
 */
public class TextPlayer extends BasicTextPlayer {


    /**
     * Construct a  TextPlayer with specified information.
     *
     * @param name        is the name of this player.
     * @param theBoard    is the board of this player.
     * @param inputReader is the input stream.
     * @param out         is the output stream.
     * @param shipFactory is the shipFactory to create ship.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory, int numMovesLeft, int numSonarLeft) {
        super(name, theBoard, inputReader, out, shipFactory, setupShipCreationList(), setupShipCreationMap(shipFactory), numMovesLeft, numSonarLeft);
    }

    /**
     * Construct a  TextPlayer with specified information.
     *
     * @param name            is the name of this player.
     * @param theBoard        is the board of this player.
     * @param inputReader     is the input stream.
     * @param out             is the output stream.
     * @param shipFactory     is the shipFactory to create ship.
     * @param ShipCreationMap is the test helper Map.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out, AbstractShipFactory<Character> shipFactory, HashMap<String, Integer> ShipCreationMap, int numMovesLeft, int numSonarLeft) {
        super(name, theBoard, inputReader, out, shipFactory, setupShipCreationListByMap(ShipCreationMap), setupShipCreationMap(shipFactory), numMovesLeft, numSonarLeft);
    }


    /**
     * A static method to set up the ship creation map.
     * @param shipFactory is the ship factory we need to use.
     * @return A hash map which maps the name and ship create function together.
     */
    public static HashMap<String, Function<Placement, Ship<Character>>> setupShipCreationMap(AbstractShipFactory<Character> shipFactory) {
        HashMap<String, Function<Placement, Ship<Character>>> tmpShipCreationFns = new HashMap<>();
        tmpShipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        tmpShipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        tmpShipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        tmpShipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        return tmpShipCreationFns;
    }


    /**
     * A test helper to generate some ship creation list.
     * @return the type of ships need to place.
     */
    public static ArrayList<String> setupShipCreationList() {
        ArrayList<String> tmpShipToPlace = new ArrayList<>();
        tmpShipToPlace.addAll(Collections.nCopies(2, "Submarine"));
        tmpShipToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        tmpShipToPlace.addAll(Collections.nCopies(3, "Battleship"));
        tmpShipToPlace.addAll(Collections.nCopies(2, "Carrier"));
        return tmpShipToPlace;
    }


    /**
     * A test helper to add specified ShipCreationList
     *
     * @param ShipCreationMap is the ship creation map.
     * @return the type of ships need to place.
     */
    public static ArrayList<String> setupShipCreationListByMap(HashMap<String, Integer> ShipCreationMap) {
        ArrayList<String> tmpShipToPlace = new ArrayList<>();
        for (String key : ShipCreationMap.keySet()) {
            tmpShipToPlace.addAll(Collections.nCopies(ShipCreationMap.get(key), key));
        }
        return tmpShipToPlace;
    }


    /**
     * Play a single turn.
     * If there is any invalid input, just ask user to start over
     * from which action type the player wants to use.
     *
     * @param enemyBoard is the enemy board
     * @param enemyName  is the enemy name
     */
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView, " Your Ocean", enemyName + "'s Ocean") + "\n");

        boolean successfullyPlayOneTurn = false;
        while (!successfullyPlayOneTurn) {
            try {
                tryPlayOneTurn(enemyBoard);
            } catch (IllegalArgumentException e) {
                out.print(e.getMessage()+"\n");
                continue;
            }
            successfullyPlayOneTurn = true;

        }
    }


    /**
     * Read a Coordinate from input source.
     *
     * @param prompt is a friendly advise for user.
     * @return Coordinate read from input source.
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if the line we read from input source is not a valid coordinate
     */
    public Coordinate readCoordinate(String prompt) throws IOException, IllegalArgumentException {
        out.print(prompt + "\n");
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("Invalid input: Empty line\n");
        }
        return new Coordinate(s);
    }

    /**
     * Read an action from Input source. Then return this action.
     * Now we have action as follows:
     * F Fire at a square
     * M Move a ship to another square (2 remaining)
     * S Sonar scan (1 remaining)
     *
     * @return The action player want to do.
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if the action not valid.
     */
    public String readAction() throws IOException, IllegalArgumentException {
        String prompt =
                "Possible actions for Player " + name + " :\n\n" +
                        " F Fire at a square\n" +
                        " M Move a ship to another square (" + numMovesLeft + " remaining)\n" +
                        " S Sonar scan (" + numSonarLeft + " remaining)\n\n" +
                        "Player " + name + ", what would you like to do?\n";
        out.print(prompt);

        HashSet<String> choices = new HashSet<String>();
        choices.add("F");
        choices.add("M");
        choices.add("S");

        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("Invalid input: Empty line\n");
        }
        s = s.toUpperCase();
        if (!choices.contains(s)) {
            throw new IllegalArgumentException("Please enter a valid choice:\n");
        }

        return s;
    }


    /**
     * Try to play one turn for this user.
     *
     * @param enemyBoard is the enemy's board.
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if any input information is invalid.
     */
    public void tryPlayOneTurn(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
        String action = readAction();

        if (action.equals("F")) {
            tryFire(enemyBoard);
        } else if (action.equals("M")) {
            tryMoveShip();
        } else {
            trySonarSan(enemyBoard);
        }

    }


    /**
     * Move any ship to another location.
     * When using this move type, a player selects one of
     * his/her own ships (the game should prompt for which
     * ship, and the player should be able to type any
     * coordinate which is a part of the ship they want).
     * The player then is prompted for a new placement
     * (location + orientation, as in initial placement).
     * The ship is then moved to that position.  Any
     * existing damage to the ship remains in the
     * same relative position(s) of the ship.
     * <p>
     * If the player selects an invalid location,
     * the player is re-prompted for their actions (and
     * might select move again, or might select a different
     * action).
     * <p>
     * Note that the other player's display does NOT gain
     * any information that the ship moved.  If they
     * previously hit that ship, the hit markers remain.
     * If they previously missed the new location, those
     * markers also remain.
     *
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if any input information(the ship which player want to move,
     *                                  the destination where player want to move their ship to) is invalid
     */
    public void tryMoveShip() throws IOException, IllegalArgumentException {

        if (numMovesLeft <= 0) {
            throw new IllegalArgumentException("All moves used. Choose a different option.");
        }

        Coordinate oriCoordinate = readCoordinate("Which ship do you want to move?\nEnter a coordinate:");

        Ship<Character> oriShip = theBoard.getShipAt(oriCoordinate);
        if (oriShip == null)
            throw new IllegalArgumentException("Invalid Coordinate: There is no ship contains this coordinate.");

        Placement destPlacement = readPlacement("Where do you want to move you " + oriShip.getName() + " to?\nEnter a placement:\n");
        Ship<Character> destShip = shipCreationFns.get(oriShip.getName()).apply(destPlacement);
        theBoard.tryMoveShip(destShip, oriShip);
        out.print("you moved the ship!\n");
        numMovesLeft -= 1;
    }

    /**
     * Try to do a sonar scan at specified coordinate(read from input source) on enemy board.
     * Sonar scan:
     * When the player selects this move type, the game
     * prompts for the center coordinates of a sonar scan.
     * Any coordinate on the game board is valid, even
     * if part of the scan will go off the edges of the
     * board.
     * <p>
     * The game then considers the following pattern
     * around (and including) the center (C)
     * <p>
     * *
     * ***
     * *****
     * ***C***
     * *****
     * ***
     * *
     * <p>
     * and reports on the number of squares occupied by each
     * type of ship in that region.   For example:
     * <p>
     * <p>
     * ---------------------------------------------------------------------------
     * Submarines occupy 2 squares
     * Destroyers occupy 0 squares
     * Battleships occupy 5 squares
     * Carriers occupy 1 square
     * ---------------------------------------------------------------------------
     * <p>
     * Note that no information about the exact position of
     * any ship in that region is report.  There is also no
     * information about how many of each ship---only how
     * many squares are occupied.  We do not know if 2
     * squares occupied by submarines is one submarine
     * entirely inside the region, or 2 submarines with
     * one square in the region and one square outside
     * the region.
     *
     * @param enemyBoard is the target board we tried to do a sonar scan at
     * @throws IllegalArgumentException if the number of sonar scan is less than 1 or
     *                                  the coordinate of sonar center is out of bound
     * @throws IOException              if read an empty line.
     */
    public void trySonarSan(Board<Character> enemyBoard) throws IllegalArgumentException, IOException {
        if (numSonarLeft <= 0) {
            throw new IllegalArgumentException("All sonar scans used. Choose a different option.");
        }
        Coordinate sonarAt = readCoordinate("Enter a coordinate for sonar scan:");
        int sonarRadius = 3;
        HashMap<String, Integer> shipCount = enemyBoard.sonar(sonarRadius, sonarAt);
        printSonarResult(shipCount);
        numSonarLeft -= 1;
    }

    /**
     * Try to fire at specified coordinate(read from input source) on enemy board.
     *
     * @param enemyBoard is the target board we tried to fire at.
     * @throws IOException              if read an empty line.
     * @throws IllegalArgumentException if the number of sonar scan is less than 1 or
     *                                  the coordinate of sonar center is out of bound
     */
    public void tryFire(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
        Coordinate hitAt = readCoordinate("Where do you want to hit your enemy? Enter a coordinate:\n");

        Ship<Character> shipHitAt = enemyBoard.fireAt(hitAt);
        if (shipHitAt == null) {
            out.print("You missed!\n");
        } else {
            out.print("You hit a " + shipHitAt.getName() + "!\n");
        }
    }

    /**
     * Print Sonar result with shipCount.
     * Such as:
     * ---------------------------------------------------------------------------
     * Submarines occupy 2 squares
     * Destroyers occupy 0 squares
     * Battleships occupy 5 squares
     * Carriers occupy 1 square
     * ---------------------------------------------------------------------------
     *
     * @param shipCount HashMap<Ship type name, number of square this type of ship occupied>
     */
    public void printSonarResult(HashMap<String, Integer> shipCount) {
        StringBuilder result = new StringBuilder();
        StringBuilder separator = new StringBuilder();
        int headerLength = 80;
        separator.append("-".repeat(headerLength));
        result.append(separator + "\n");
        result.append("Submarines occupy " + shipCount.get("Submarine") + " squares\n");
        result.append("Destroyers occupy " + shipCount.get("Destroyer") + " squares\n");
        result.append("Battleships occupy " + shipCount.get("Battleship") + " squares\n");
        result.append("Carriers occupy " + shipCount.get("Carrier") + " squares\n");
        result.append(separator);
        out.print(result + "\n");
    }

    /**
     * Print Sonar result with shipCount.
     * Such as:
     * ---------------------------------------------------------------------------
     * Submarines occupy 2 squares
     * Destroyers occupy 0 squares
     * Battleships occupy 5 squares
     * Carriers occupy 1 square
     * ---------------------------------------------------------------------------
     *
     * @param shipCount HashMap<Ship type name, number of square this type of ship occupied>
     */
    public String generateSonarResult(HashMap<String, Integer> shipCount) {
        StringBuilder result = new StringBuilder();
        StringBuilder separator = new StringBuilder();
        int headerLength = 5;
        separator.append("-".repeat(headerLength));
        result.append(separator + "\n");
        result.append("Submarines occupy " + shipCount.get("Submarine") + " squares\n");
        result.append("Destroyers occupy " + shipCount.get("Destroyer") + " squares\n");
        result.append("Battleships occupy " + shipCount.get("Battleship") + " squares\n");
        result.append("Carriers occupy " + shipCount.get("Carrier") + " squares\n");
        result.append(separator);
        return result.toString();
    }

    /**
     * Check if the player loses.
     * Which means all his ship are sunk.
     *
     * @return true if the player loses, false otherwise
     */
    public boolean checkLose() {
        return theBoard.isAllSunk();
    }


    /**
     * Declare this player win the game!
     */
    @Override
    public void declareWinner() {
        out.print("Congratulations! Player " + getName() + " win!\n");
    }
}
