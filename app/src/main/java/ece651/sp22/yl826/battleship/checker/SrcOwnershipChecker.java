package ece651.sp22.yl826.battleship.checker;

import ece651.sp22.yl826.battleship.order.Order;
import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.territory.Territory;

public class SrcOwnershipChecker extends ActionChecker {

  /**
   * check if the source territory is owned by this player
   * @param riskMap
   * @param moveOrder
   * @return
   */
  @Override
  protected String checkMyRule(RISKMap riskMap, Order moveOrder) {
    Territory src = riskMap.getTerritoryByName(moveOrder.getSrcTerritory());
    if (src.getOwnerID() != moveOrder.getPlayerID()){
      return "You must place orders from your own territories!";
    }
    return null;
  }

  public SrcOwnershipChecker(ActionChecker next) {
    super(next);
  }

}
