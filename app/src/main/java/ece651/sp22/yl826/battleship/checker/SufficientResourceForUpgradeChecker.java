package ece651.sp22.yl826.battleship.checker;

import ece651.sp22.yl826.battleship.order.Order;
import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.unit.BasicUnit;
import ece651.sp22.yl826.battleship.unit.Unit;

/**
 * Checker for resource to upgrade unit.
 */
public class SufficientResourceForUpgradeChecker extends ActionChecker{

    public SufficientResourceForUpgradeChecker(ActionChecker next) {
        super(next);
    }

    /**
     * check if this player has enough resource to operate unit upgrade order
     * @param riskMap
     * @param unitUpgradeOrder
     * @return
     */
    @Override
    protected String checkMyRule(RISKMap riskMap, Order unitUpgradeOrder) {
        Unit toUpgrade = new BasicUnit(unitUpgradeOrder.getUnitType(), unitUpgradeOrder.getUnitAmount());
        if (unitUpgradeOrder.getToLevel() <= toUpgrade.getLevel() || unitUpgradeOrder.getToLevel() > toUpgrade.getLevelBound()) {
            return toUpgrade.getType() + " level " + toUpgrade.getLevel() + " cannot upgrade to " + unitUpgradeOrder.getToLevel();
        }
        int techCost = toUpgrade.getAmount() * toUpgrade.getCostToLevel(unitUpgradeOrder.getToLevel());
        if (techCost > riskMap.getOwners().get(unitUpgradeOrder.getPlayerID()).getOwnedTechResource()) {
            return "Insufficient resource to upgrade " + toUpgrade;
        }
        return null;
    }
}
