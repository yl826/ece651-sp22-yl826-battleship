package ece651.sp22.yl826.battleship.checker;

import ece651.sp22.yl826.battleship.map.RISKMap;
import ece651.sp22.yl826.battleship.order.Order;

public class PathResourceAttackChecker extends ActionChecker {

    /**
     * check if the attack order has enough resources to operate on
     * @param riskMap
     * @param moveOrder
     * @return null if all rules passed; return error message if rule didn't pass
     */
    @Override
    protected String checkMyRule(RISKMap riskMap, Order moveOrder) {
        int cost = -1*moveOrder.getUnitAmount();

        return riskMap.getOwners().get(moveOrder.getPlayerID()).tryAddOrRemoveFoodResource(cost);
    }
    public PathResourceAttackChecker(ActionChecker next) {
        super(next);
    }
}
