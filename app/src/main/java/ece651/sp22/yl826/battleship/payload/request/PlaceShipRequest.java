package ece651.sp22.yl826.battleship.payload.request;

import java.util.ArrayList;

public class PlaceShipRequest {
    private Long roomID;
    private ArrayList<PlaceShip> shipPlaceOrders;

    public Long getRoomID() {
        return roomID;
    }

    public void setRoomID(Long roomID) {
        this.roomID = roomID;
    }

    public ArrayList<PlaceShip> getShipPlaceOrders() {
        return shipPlaceOrders;
    }

    public void setShipPlaceOrders(ArrayList<PlaceShip> shipPlaceOrders) {
        this.shipPlaceOrders = shipPlaceOrders;
    }
}
