package ece651.sp22.yl826.battleship.payload.response;

public class RoomsAvailable {
    Long roomID;

    public Long getRoomID() {
        return roomID;
    }

    public void setRoomID(Long roomID) {
        this.roomID = roomID;
    }

    public Integer getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(Integer roomSize) {
        this.roomSize = roomSize;
    }

    public RoomsAvailable(Long roomID, Integer roomSize) {
        this.roomID = roomID;
        this.roomSize = roomSize;
    }

    Integer roomSize;
}
