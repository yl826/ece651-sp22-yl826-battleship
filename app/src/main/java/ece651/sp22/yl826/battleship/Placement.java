package ece651.sp22.yl826.battleship;

import java.util.Objects;

/**
 * This class store the information for a placement.
 */
public class Placement {
    /**
     * coordinate for this placement.
     */
    private final Coordinate where;

    /**
     * orientation for this placement.
     */
    private final char orientation;

    /**
     * Constructs a Placement with the specified coordinate and orientation.
     * @param where is the coordinate of the newly constructed placement.
     * @param orientation is the orientation of the newly constructed placement.
     */
    public Placement(Coordinate where, char orientation) {
        this.where = where;
        this.orientation = orientation;
    }

    /**
     * Constructs a Placement with the specified coordinate and orientation(Using String format such as "A5V" for row(0), column(5), orientation(V)).
     * @param s is the string format of the coordinate and orientation.
     * @throws IllegalArgumentException if the param s is not valid for converting to number.
     */
    public Placement(String s){
        String UpperCaseS = s.toUpperCase();
        if (UpperCaseS.length() != 3) {
            throw new IllegalArgumentException("Invalid string length for Placement: " + s);
        }
        this.where = new Coordinate(UpperCaseS.substring(0, 2));
        if (UpperCaseS.charAt(2) < 'A' || UpperCaseS.charAt(2) > 'Z') {
            throw new IllegalArgumentException("Invalid char for orientation: " + s);
        }
        this.orientation = UpperCaseS.charAt(2);
    }

    public Coordinate getWhere() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    /**
     *
     * @param o Another Object need to compare with.
     * @return true if this is numerically equal in coordinate to the other and alphabetically equal in orientation.
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement c = (Placement) o;
            return orientation == c.orientation && where.equals(c.where);
        }
        return false;
    }

    /**
     *
     * @return Using the String's hashCode method to generate hashCode.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }


    /**
     *
     * @return Representing the placement object with a string like "Placement{where=(row, column), orientation=orientation}".
     */
    @Override
    public String toString() {
        return "Placement{" +
                "where=" + where +
                ", orientation=" + orientation +
                '}';
    }
}
