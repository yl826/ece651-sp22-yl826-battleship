package ece651.sp22.yl826.battleship;


/**
 * Check the validity of any placement by rule chain.
 *
 * @param <T> The type of ship and Board.
 */
public abstract class PlacementRuleChecker<T> {


    /**
     * The next rule to heck.
     */
    private final PlacementRuleChecker<T> next;

    /**
     * Construct a PlacementRuleChecker with specified next.
     *
     * @param next is the next PlacementRuleChecker need to check.
     */
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * The rule need tocheck.
     *
     * @param theShip  is the ship need to check.
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * The rule need tocheck.
     *
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    protected abstract String checkMyRule(Ship<T> oriShip, Ship<T> destShip, Board<T> theBoard);

    /**
     * The rule need tocheck.
     *
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    protected abstract String checkMyRule(Coordinate theCoordinate, Board<T> theBoard);


    /**
     * Check the validity of any placement by rule chain.
     *
     * @param theShip  is the ship need to check.
     * @param theBoard id the board need to check.
     * @return ture if thsShip can be place on theBoard, false otherwise.
     */
    public String checkPlacement(Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String errorMessage = checkMyRule(theShip, theBoard);
        if (errorMessage != null) {
            return errorMessage;
        }
        //other wise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        //if there are no more rules, then the placement is legal
        return null;
    }

    /**
     * Check if a movement is valid or not.
     * @param oriShip is the ori ship we need to check with.
     * @param destShip is the dest ship we need to check with.
     * @param theBoard is the board we need to check with.
     * @return null if no invalid situation, otherwise a String describe the error.
     */
    public String checkPlacement(Ship<T> oriShip, Ship<T> destShip, BattleShipBoard<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String errorMessage = checkMyRule(oriShip, destShip, theBoard);
        if (errorMessage != null) {
            return errorMessage;
        }
        //other wise, ask the rest of the chain.
        //TODO Jetbrains Code Coverage Tool always show uncover branch if we check next==null as above. But why?
        return next.checkMyRule(oriShip, destShip, theBoard);

    }

    /**
     * Check if a coordinate is valid or not.
     * @param c is the coordinate we need to check with.
     * @param theBoard is the board we need to check with.
     * @return null if no invalid situation, otherwise a String describe the error.
     */
    public String checkPlacement(Coordinate c, BattleShipBoard<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String errorMessage = checkMyRule(c, theBoard);
        if (errorMessage != null) {
            return errorMessage;
        }
        //other wise, ask the rest of the chain.
        //TODO Jetbrains Code Coverage Tool always show uncover branch if we check next==null as above. But why?
        return next.checkMyRule(c, theBoard);

    }


}
