package ece651.sp22.yl826.battleship;

import java.util.HashMap;

/**
 * This interface represents any type of Board in our Battleship game. It is
 * generic in typename T, which is the type of information the view needs to
 * display this ship.
 */
public interface Board<T> {
    public int getWidth();

    public int getHeight();

    /**
     * Try to add ship toAdd to the Board object.
     * @param toAdd A ship need to add.
     * @return True if we successfully add this ship, otherwise False.
     */
    public String tryAddShip(Ship<T> toAdd);

    /**
     * Return the view-specific information at the given coordinate. Null if none is fund ar the location.
     * @param where is the coordinate to return information for
     * @return The view-specific information at that coordinate.
     */
    public T whatIsAtForSelf(Coordinate where);

    /**
     * Search for any ship that occupies coordinate c
     * If one is found, that Ship is "hit" by the attack and should record it.
     *
     * @param c the coordinate of fire
     * @return The ship fire at or null if no ship at c.
     */
    public Ship<T> fireAt(Coordinate c);

    /**
     * Return the view-specific information at the given coordinate. Null if none is fund ar the location.
     *
     * @param where is the coordinate to return information for
     * @return The view-specific information at that coordinate.
     */
    public T whatIsAtForEnemy(Coordinate where);

    /**
     * Check if all of player's is sunk. Which means there is a winner.
     * @return True if all the ship player has is sunk, false otherwise.
     */
    public boolean isAllSunk();

    /**
     * Try to move a ship.
     * @param toAdd the ship to add
     * @param oriShip
     */
    public void tryMoveShip(Ship<T> toAdd, Ship<T> oriShip);

    /**
     * Get ship at specified coordinate
     * @param where the coordinate to check
     * @return ship at where
     */
    public Ship<T> getShipAt(Coordinate where);

    /**
     * Sonar scan: returns number of squares occupied by each ship
     * present around a coordinate C in the following pattern:
     *                 *
     *                ***
     *               *****
     *              ***C***
     *               *****
     *                ***
     *                 *
     * @param where is the center coordinate C around which to find ships
     * @returns a hashmap of ship name and the number of squares occupied by it.
     * @throws IllegalArgumentException if C is not part of the battleship
     */
    public HashMap<String, Integer> sonar(int range, Coordinate where) throws IllegalArgumentException;

}
