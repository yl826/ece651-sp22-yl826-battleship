package ece651.sp22.yl826.battleship.payload.request;

public class PlaceOrder {

    String col;
    String row;
    String orderType;
    String source_col;
    String source_row;
    String ori;

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSource_col() {
        return source_col;
    }

    public void setSource_col(String source_col) {
        this.source_col = source_col;
    }

    public String getSource_row() {
        return source_row;
    }

    public void setSource_row(String source_row) {
        this.source_row = source_row;
    }

    public String getOri() {
        return ori;
    }

    public void setOri(String ori) {
        this.ori = ori;
    }

    public PlaceOrder(String col, String row, String orderType, String source_col, String source_row, String ori) {
        this.col = col;
        this.row = row;
        this.orderType = orderType;
        this.source_col = source_col;
        this.source_row = source_row;
        this.ori = ori;
    }

    public PlaceOrder() {
    }
}
