package ece651.sp22.yl826.battleship;


import java.util.HashMap;

/**
 * This class represents the BattleShip
 *
 * @param <T> Type of info to display with.
 */
public class BattleShip<T> extends BasicShip<T> {
    /**
     * The name of this ship. Such as "submarine".
     */
    private final String name;

    /**
     * Generate all the coordinate where the rectangleShip takes based on the arguments.
     *
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @return A set of coordinate which contains all the coordinate of this rectangle ship.
     */
    static HashMap<Coordinate, Integer> makeCoords(Coordinate upperLeft, char orientation) {
        HashMap<Coordinate, Integer> myCoords = new HashMap<>();
        if (orientation == 'U') {
            //convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 1), 0);
            //start from the left side of convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0), 1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1), 2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2), 3);
        }
        if (orientation == 'R') {
            //convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),0);
            //start from the left side of convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 0),1);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 0),3);
        }
        if (orientation == 'D') {
            //convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),0);
            //start from the left side of convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 0),3);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 1),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 2),1);

        }
        if (orientation == 'L') {
            //convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 0),0);
            //start from the left side of convex b
            myCoords.put(new Coordinate(upperLeft.getRow() + 0, upperLeft.getColumn() + 1),3);
            myCoords.put(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1),2);
            myCoords.put(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1),1);
        }

        return myCoords;
    }

    /**
     * Construct a rectangle ship with specified arguments.
     *
     * @param upperLeft       is the upper-left corner coordinate of this rectangle ship.
     * @param shipDisplayInfo is the info need to display this ship.
     */
    public BattleShip(String name, Coordinate upperLeft, ShipDisplayInfo<T> shipDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, char orientation) {
        super(new Placement(upperLeft, orientation), makeCoords(upperLeft, orientation), shipDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Construct a rectangle ship with specified arguments.
     *
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param data      is the info need to display in normal condition.
     * @param onHit     is the display info when hit.
     */
    public BattleShip(String name, Coordinate upperLeft, T data, T onHit, char orientation) {
        this(name, upperLeft, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data), orientation);
    }




    /**
     * Get the name of this Ship, such as "submarine".
     *
     * @return the name of this ship
     */
    @Override
    public String getName() {
        return name;
    }

    public void aMethodNotCoverByTest_ForGitLabCICDMergeCodeCoverageTest(){
        // do nothing
    }




}
