/*
 * A text-based version of the game "battleship".
 */
package ece651.sp22.yl826.battleship;

import ece651.sp22.yl826.battleship.player.TextPlayer;
import ece651.sp22.yl826.battleship.player.TextPlayerAI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    /**
     * The first battleship game player.
     */
    final TextPlayer player1;


    /**
     * The second  battleship game player.
     */
    final TextPlayer player2;


    /**
     * Constructs an App with the specified information show as below.
     *
     * @param player1 is the first battleship game player.
     * @param player2 is the second battleship game player.
     */
    public App(TextPlayer player1, TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }


    /**
     * Do the entire placement phase for both players.
     *
     * @throws IOException if the input source is invalid.
     */
    public void doOnePlacement() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();

    }


    /**
     * Do the entire attach phase for both players.
     * @throws IOException if the input source is invalid.
     */
    public void doAttackPhase() throws IOException {
        do {
            player1.playOneTurn(player2.getTheBoard(), player2.getName());
            if (player2.checkLose()) {
                player1.declareWinner();
                break;
            }
            player2.playOneTurn(player1.getTheBoard(), player1.getName());
            if (player1.checkLose()) {
                player2.declareWinner();
                break;
            }
        } while (true);
    }



    /**
     * Construct a new Battleship game with a Board width 10, height 20.
     *
     * @param args Arguments.
     * @throws IOException if the input source is invalid.
     */
    public static void main(String[] args) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        AbstractShipFactory<Character> abstractShipFactory = new V2ShipFactory();

        boolean isPlayer1AI = false;
        boolean isPlayer2AI = false;
        while (true) {
            System.out.print("Who will play Player A?\nEnter Human or AI:\n");
            String s = bufferedReader.readLine();
            if (s.toUpperCase().equals("HUMAN") || s.toUpperCase().equals("AI")) {
                isPlayer1AI = s.toUpperCase().equals("AI");
                break;
            } else {
                System.out.print("Invalid role type.\n");
            }
        }

        while (true) {
            System.out.print("Who will play Player B?\nEnter Human or AI:\n");
            String s = bufferedReader.readLine();
            if (s.toUpperCase().equals("HUMAN") || s.toUpperCase().equals("AI")) {
                isPlayer2AI = s.toUpperCase().equals("AI");
                break;
            } else {
                System.out.print("Invalid role type.\n");
            }
        }
        TextPlayer player1;
        if (isPlayer1AI) {
            player1 = new TextPlayerAI("A", new BattleShipBoard<>(10, 20, 'X'),
                    bufferedReader, System.out, abstractShipFactory, 3, 3);
        } else {
            player1 = new TextPlayer("A", new BattleShipBoard<>(10, 20, 'X'),
                    bufferedReader, System.out, abstractShipFactory, 3, 3);
        }

        TextPlayer player2;
        if (isPlayer2AI) {
            player2 = new TextPlayerAI("B", new BattleShipBoard<>(10, 20, 'X'),
                    bufferedReader, System.out, abstractShipFactory, 3, 3);
        } else {
            player2 = new TextPlayer("B", new BattleShipBoard<>(10, 20, 'X'),
                    bufferedReader, System.out, abstractShipFactory, 3, 3);
        }

        App app = new App(player1, player2);

        app.doOnePlacement();
        app.doAttackPhase();

    }
    public void aMethodNotCoverByTest_ForGitLabCICDMergeCodeCoverageTest(){
        // do nothing
    }

}
