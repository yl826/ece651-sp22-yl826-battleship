package ece651.sp22.yl826.battleship.factory;

import ece651.sp22.yl826.battleship.map.GameMap;

public interface AbstractMapFactory {
  /**
   * Create random map for specific number of players
   * @param n: int: number of players
   * @return
   */
  public GameMap createMapForNplayers(int n);
}
