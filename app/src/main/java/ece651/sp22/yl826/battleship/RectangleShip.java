package ece651.sp22.yl826.battleship;

import java.util.HashMap;
import java.util.HashSet;


/**
 * This class represents the rectangle type for a Ship.
 * @param <T> Type of info to display with.
 */
public class RectangleShip<T> extends BasicShip<T> {

    /**
     * The name of this ship. Such as "submarine".
     */
    private final String name;

    /**
     * Generate all the coordinate where the rectangleShip takes based on the arguments.
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param width is the width of this rectangle ship.
     * @param height is the height of this rectangle ship.
     * @return A set of coordinate which contains all the coordinate of this rectangle ship.
     */
    static HashMap<Coordinate, Integer> makeCoords(Coordinate upperLeft, int width, int height) {
        HashMap<Coordinate, Integer> myCoords = new HashMap<>();
        int cnt = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                myCoords.put(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j),cnt);
                cnt+=1;
            }
        }
        return myCoords;
    }


    /**
     * Construct a rectangle ship with specified arguments.
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param width is the width of this rectangle ship.
     * @param height is the height of this rectangle ship.
     * @param shipDisplayInfo is the info need to display this ship.
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> shipDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(new Placement(upperLeft,'V'),makeCoords(upperLeft, width, height), shipDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Construct a rectangle ship with specified arguments.
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param width is the width of this rectangle ship.
     * @param height is the height of this rectangle ship.
     * @param data is the info need to display in normal condition.
     * @param onHit is the display info when hit.
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Construct a basic ship with 1*1 size and name"testShip".
     * @param upperLeft is the upper-left corner coordinate of this rectangle ship.
     * @param data is the info need to display in normal condition.
     * @param onHit is the display info when hit.
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    /**
     * Get the name of this Ship, such as "submarine".
     *
     * @return the name of this ship
     */
    @Override
    public String getName() {
        return name;
    }

}
