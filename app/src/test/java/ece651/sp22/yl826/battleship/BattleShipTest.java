package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BattleShipTest {
    @Test
    void makeCoords() {
        HashMap<Coordinate, Integer> map = BattleShip.makeCoords(new Coordinate(0, 0), 'U');
        Set<Coordinate> c= map.keySet();
        assertEquals(c, new HashSet<Coordinate>(){{
            add(new Coordinate(0, 1));
            add(new Coordinate(1, 0));
            add(new Coordinate(1, 1));
            add(new Coordinate(1, 2));
        }});

        c = BattleShip.makeCoords(new Coordinate(0, 0), 'R').keySet();
        assertEquals(c, new HashSet<Coordinate>(){{
            add(new Coordinate(0, 0));
            add(new Coordinate(1, 0));
            add(new Coordinate(2, 0));
            add(new Coordinate(1, 1));
        }});

        c = BattleShip.makeCoords(new Coordinate(0, 0), 'D').keySet();
        assertEquals(c, new HashSet<Coordinate>(){{
            add(new Coordinate(0, 0));
            add(new Coordinate(0, 1));
            add(new Coordinate(0, 2));
            add(new Coordinate(1, 1));
        }});

        c = BattleShip.makeCoords(new Coordinate(0, 0), 'L').keySet();
        assertEquals(c, new HashSet<Coordinate>(){{
            add(new Coordinate(0, 1));
            add(new Coordinate(1, 0));
            add(new Coordinate(1, 1));
            add(new Coordinate(2, 1));
        }});


    }


    @Test
    public void getName(){
        Placement p1 = new Placement("A0U");
        BattleShip<Character> sU = new BattleShip<Character>("testship", p1.getWhere(), 's', '*',p1.getOrientation());
        assertEquals("testship",sU.getName());
    }






}