package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class V1ShipFactoryTest {

    @Test
    void makeSubmarine() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        checkShip(dst, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

    }

    @Test
    void makeBattleship() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeBattleship(v1_2);
        checkShip(dst, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));

    }

    @Test
    void makeCarrier() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst = f.makeCarrier(v1_2);
        checkShip(dst, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));

    }

    @Test
    void makeDestroyer() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    }

    @Test
    void createShip() {
    }

    /**
     * Check all coordinates and ship name.
     * @param testShip is the ship need to check.
     * @param expectedName is the expected name of the ship.
     * @param expectedLetter is the expected display letter of the ship.
     * @param expectedLocs is an array of the expected locations of this ship.
     */
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c: expectedLocs)
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c,true));

    }
}