package ece651.sp22.yl826.battleship;

import ece651.sp22.yl826.battleship.player.TextPlayer;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TextPlayerTest {
    @Test
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');
        for (Placement placement : expected) {

            Placement p = player.readPlacement(prompt);
            assertEquals(p, placement); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //F**K the CRLF!should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }

    }

    @Test
    void test_read_empty_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "", bytes);
        assertThrows(EOFException.class, () -> player.readPlacement(""));


    }

    @Test
    void test_do_one_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);


        String expected[] = new String[3];
        expected[0] = "Player A where do you want to place a Destroyer?\n" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |d| | | | | | |  B\n" +
                "C  | |d| | | | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";
        expected[1] = "Player A where do you want to place a Destroyer?\n" +
                "That placement is invalid: the ship goes off the right of the board.\n" +
                "Player A where do you want to place a Destroyer?\n" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | |d| | | | |  A\n" +
                "B  | |d| |d| | | | |  B\n" +
                "C  | |d| |d| | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";
        expected[2] = "Player A where do you want to place a Destroyer?\n" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | |s| | | | |  A\n" +
                "B  | |s| |s| | | | |  B\n" +
                "C  | |s| |s| | | | |  C\n" +
                "D  | |s| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";


        player.doOnePlacement("Destroyer", player.getShipCreationFns().get("Destroyer"));
        assertEquals(expected[0] + "", bytes.toString()); //F**K the CRLF!should have printed prompt and newline
        bytes.reset(); //clear out bytes for next time around

        player.doOnePlacement("Destroyer", player.getShipCreationFns().get("Destroyer"));
        assertEquals(expected[1] + "", bytes.toString()); //F**K the CRLF!should have printed prompt and newline
        bytes.reset(); //clear out bytes for next time around

    }

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        HashMap<String, Integer> ShipCreationMap = new HashMap<>();
        ShipCreationMap.put("Destroyer", 1);
        return new TextPlayer("A", board, input, output, shipFactory, ShipCreationMap, 3, 3);
    }

    private TextPlayer createTextPlayerWithV2Factory(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        HashMap<String, Integer> ShipCreationMap = new HashMap<>();
        ShipCreationMap.put("Destroyer", 1);
        return new TextPlayer("A", board, input, output, shipFactory, ShipCreationMap, 3, 3);
    }
    private TextPlayer createTextPlayerWithSpecifiedNumOfAction(int w, int h, String inputData, OutputStream bytes, int numOfMove, int numOfSonar) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        HashMap<String, Integer> ShipCreationMap = new HashMap<>();
        ShipCreationMap.put("Destroyer", 1);
        return new TextPlayer("A", board, input, output, shipFactory, ShipCreationMap, numOfMove, numOfSonar);
    }

    @Test
    void test_doOnePlacementPhase() throws IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\na4v\na4v\na4v\n", bytes);

        String helpMessage =
                "\n" +
                        "Player A: you are going to place the following ships (which are all\n" +
                        "rectangular). For each ship, type the coordinate of the upper left\n" +
                        "side of the ship, followed by either H (for horizontal) or V (for\n" +
                        "vertical).  For example M4H would place a ship horizontally starting\n" +
                        "at M4 and going to the right.  You have\n" +
                        "\n" +
                        "2 \"Submarines\" ships that are 1x2\n" +
                        "3 \"Destroyers\" that are 1x3\n" +
                        "3 \"Battleships\" that are 1x4\n" +
                        "2 \"Carriers\" that are 1x6\n" +
                        "Player A where do you want to place a Destroyer?\n";


        String[] expected = new String[4];

        expected[0] = "" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | | | | | | | | |  B\n" +
                "C  | | | | | | | | |  C\n" +
                "D  | | | | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";

        expected[1] = "" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |d| | | | | | |  B\n" +
                "C  | |d| | | | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";
        expected[2] = "" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | |s| | | | |  A\n" +
                "B  | |s| |s| | | | |  B\n" +
                "C  | |s| |s| | | | |  C\n" +
                "D  | |s| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";
        expected[3] = "" +
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | |s| | | | |  A\n" +
                "B  | |s| |s| | | | |  B\n" +
                "C  | |s| |s| | | | |  C\n" +
                "D  | |s| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";


        player.doPlacementPhase();

        assertEquals(expected[0] + helpMessage + "" + expected[1] + "", bytes.toString());


        bytes.reset(); //clear out bytes for next time around


    }

    @Test
    void test_doOnePlacementPhaseWithEmptyLine() throws IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        TextPlayer player = createTextPlayer(10, 20, "", bytes);


        assertThrows(EOFException.class, () -> player.doPlacementPhase());

    }

    @Test
    void setupShipCreationList() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\na4v\na4v\na4v\n", bytes);
        ArrayList<String> actualRes = player.setupShipCreationList();
        ArrayList<String> expectedRes = new ArrayList<>();
        expectedRes.addAll(Collections.nCopies(2, "Submarine"));
        expectedRes.addAll(Collections.nCopies(3, "Destroyer"));
        expectedRes.addAll(Collections.nCopies(3, "Battleship"));
        expectedRes.addAll(Collections.nCopies(2, "Carrier"));
        assertEquals(expectedRes, actualRes);
    }


    @Test
    void playOneTurn() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 4, "F\nA0\n", bytes);
        Board<Character> enemyBoard = new BattleShipBoard<Character>(4, 4, 'X');
        V1ShipFactory f = new V1ShipFactory();
        enemyBoard.tryAddShip(f.makeSubmarine(new Placement("A0H")));
        try {
            player.playOneTurn(enemyBoard, "enemy");
        } catch (IOException e) {

        }

        String expectedString = "      Your Ocean              enemy's Ocean\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "A  | | |  A                A  | | |  A\n" +
                "B  | | |  B                B  | | |  B\n" +
                "C  | | |  C                C  | | |  C\n" +
                "D  | | |  D                D  | | |  D\n" +
                "  0|1|2|3                    0|1|2|3\n" +
                "\n" +
                "Possible actions for Player A :\n" +
                "\n" +
                " F Fire at a square\n" +
                " M Move a ship to another square (3 remaining)\n" +
                " S Sonar scan (3 remaining)\n" +
                "\n" +
                "Player A, what would you like to do?\n" +
                "Where do you want to hit your enemy? Enter a coordinate:\n" +
                "\n" +
                "You hit a Submarine!\n";

        assertEquals(expectedString, bytes.toString());

    }

    @Test
    void tryMove() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithV2Factory(5, 5, "F\nA0\n", bytes);
        TextPlayer player2 = createTextPlayerWithV2Factory(5, 5, "M\nA0\nA0H\nM\nA2\nC0L\n", bytes);
        //Board<Character> enemyBoard = new BattleShipBoard<Character>(4, 4, 'X');
        V2ShipFactory f = new V2ShipFactory();
        player2.getTheBoard().tryAddShip(f.makeSubmarine(new Placement("A0V")));

        try {
            player1.playOneTurn(player2.getTheBoard(), "enemy");
        } catch (IOException e) {

        }
        String expectedAfterFireAtA0 =
                "  0|1|2|3|4\n" +
                        "A *| | | |  A\n" +
                        "B s| | | |  B\n" +
                        "C  | | | |  C\n" +
                        "D  | | | |  D\n" +
                        "E  | | | |  E\n" +
                        "  0|1|2|3|4\n";
        assertEquals(expectedAfterFireAtA0, player2.getView().displayMyOwnBoard());

        try {
            player2.playOneTurn(player1.getTheBoard(), "enemy");
        } catch (IOException e) {

        }
        String expectedAfterFireAtA0AndMoveToA0H =
                "  0|1|2|3|4\n" +
                        "A *|s| | |  A\n" +
                        "B  | | | |  B\n" +
                        "C  | | | |  C\n" +
                        "D  | | | |  D\n" +
                        "E  | | | |  E\n" +
                        "  0|1|2|3|4\n";
        assertEquals(expectedAfterFireAtA0AndMoveToA0H, player2.getView().displayMyOwnBoard());

        //Test for Carriers
        player2.getTheBoard().tryAddShip(f.makeCarrier(new Placement("A2U")));
        String expected =
                "  0|1|2|3|4\n" +
                        "A *|s|c| |  A\n" +
                        "B  | |c| |  B\n" +
                        "C  | |c|c|  C\n" +
                        "D  | |c|c|  D\n" +
                        "E  | | |c|  E\n" +
                        "  0|1|2|3|4\n";
        assertEquals(expected, player2.getView().displayMyOwnBoard());

        player2.getTheBoard().fireAt(new Coordinate("A2"));
        player2.getTheBoard().fireAt(new Coordinate("D2"));
        player2.getTheBoard().fireAt(new Coordinate("D3"));
        expected =
                "  0|1|2|3|4\n" +
                        "A *|s|*| |  A\n" +
                        "B  | |c| |  B\n" +
                        "C  | |c|c|  C\n" +
                        "D  | |*|*|  D\n" +
                        "E  | | |c|  E\n" +
                        "  0|1|2|3|4\n";
        assertEquals(expected, player2.getView().displayMyOwnBoard());

        bytes.reset();
        try {
            player2.playOneTurn(player1.getTheBoard(), "enemy");
        } catch (Exception e) {
        }

        expected =
                "  0|1|2|3|4\n" +
                        "A *|s| | |  A\n" +
                        "B  | | | |  B\n" +
                        "C  | |c|*|c C\n" +
                        "D *|c|c|*|  D\n" +
                        "E  | | | |  E\n" +
                        "  0|1|2|3|4\n";
        //assertEquals("",bytes);
        assertEquals(expected, player2.getView().displayMyOwnBoard());

    }

    @Test
    void test_Empty_Action() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithV2Factory(5, 5, "", bytes);

        assertThrows(EOFException.class, () -> player1.playOneTurn(player1.getTheBoard(), "enemy"));

    }

    @Test
    void test_Empty_Action_Coordinate() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithV2Factory(5, 5, "F\n", bytes);

        assertThrows(EOFException.class, () -> player1.playOneTurn(player1.getTheBoard(), "enemy"));

    }

    @Test
    void test_Move_Null_Ori() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithV2Factory(5, 5, "M\nA0\n", bytes);

        assertThrows(EOFException.class, () -> player1.playOneTurn(player1.getTheBoard(), "enemy"));

    }

    @Test
    void test_Move_No_Move_Left() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithSpecifiedNumOfAction(5, 5, "M\nA0\nA0H\nM\nA0\nA0V\nM\nA0\nA0H\nM\nA0\nA0H\n", bytes,0,0);
        V2ShipFactory f = new V2ShipFactory();
        Ship<Character> A0V = f.makeSubmarine(new Placement("A0V"));
        Ship<Character> A0H = f.makeSubmarine(new Placement("A0V"));
        player1.getTheBoard().tryAddShip(A0V);

        assertThrows(IllegalArgumentException.class,()->player1.tryMoveShip());
    }

    @Test
    void test_Sonar_No_Sonar_Left() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayerWithSpecifiedNumOfAction(5, 5, "M\nA0\nA0H\nM\nA0\nA0V\nM\nA0\nA0H\nM\nA0\nA0H\n", bytes,0,0);

        assertThrows(IllegalArgumentException.class,()->player1.trySonarSan(player1.getTheBoard()));
    }
}