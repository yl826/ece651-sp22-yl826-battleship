package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {

    @Test
    void makeCoords() {
        Set<Coordinate> c = RectangleShip.makeCoords(new Coordinate(1, 2), 1, 3).keySet();
        assertEquals(c, new HashSet<Coordinate>(){{
            add(new Coordinate(1, 2));
            add(new Coordinate(2, 2));
            add(new Coordinate(3, 2));
        }});
    }

    @Test
    void isSunk() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship", new Coordinate(0, 0), 3, 2, 's', '*');
        rectangleShip.recordHitAt(new Coordinate(0,0));
        rectangleShip.recordHitAt(new Coordinate(0,1));
        rectangleShip.recordHitAt(new Coordinate(0,2));
        rectangleShip.recordHitAt(new Coordinate(1,0));
        rectangleShip.recordHitAt(new Coordinate(1,1));
        assertFalse(rectangleShip.isSunk());
        rectangleShip.recordHitAt(new Coordinate(1,2));
        assertTrue(rectangleShip.isSunk());
    }

    @Test
    void recordHit() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship",new Coordinate(0, 0), 3, 2, 's', '*');
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.recordHitAt(new Coordinate(2,1)));

        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),true));
        assertEquals(null, rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),false));
        rectangleShip.recordHitAt(new Coordinate(0, 0));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),false));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(0, 1),true));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(0, 2),true));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(1, 0),true));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(1, 1),true));
        assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(1, 2),true));

        rectangleShip.recordHitAt(new Coordinate(0,0));
        assertEquals('*', rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),true));

        rectangleShip.recordHitAt(new Coordinate(1,0));
        assertEquals('*', rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),true));


        rectangleShip.recordHitAt(new Coordinate(1,1));
        assertEquals('*', rectangleShip.getDisplayInfoAt(new Coordinate(0, 0),true));

    }

    @Test
    void wasHitAt() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship",new Coordinate(0, 0), 3, 2, 's', '*');
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.wasHitAt(new Coordinate(2,1)));

        assertFalse(rectangleShip.wasHitAt(new Coordinate(0, 0)));

        rectangleShip.recordHitAt(new Coordinate(0,0));
        assertTrue(rectangleShip.wasHitAt(new Coordinate(0, 0)));

        rectangleShip.recordHitAt(new Coordinate(1,0));
        assertTrue(rectangleShip.wasHitAt(new Coordinate(0, 0)));


        rectangleShip.recordHitAt(new Coordinate(1,0));
        assertTrue(rectangleShip.wasHitAt(new Coordinate(0, 0)));
    }

    @Test
    void getName() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship",new Coordinate(0, 0), 3, 2, 's', '*');
        assertEquals("test 3*2 ship",rectangleShip.getName());
    }

    @Test
    void getCiirdubates() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship",new Coordinate(0, 0), 3, 2, 's', '*');
        HashSet<Coordinate> expectedSet = new HashSet<>();
        expectedSet.add(new Coordinate(0, 0));
        expectedSet.add(new Coordinate(0, 1));
        expectedSet.add(new Coordinate(0, 2));
        expectedSet.add(new Coordinate(1, 0));
        expectedSet.add(new Coordinate(1, 1));
        expectedSet.add(new Coordinate(1, 2));
        for (Coordinate c : rectangleShip.getCoordinates()) {
            assertTrue(expectedSet.contains(c));
            expectedSet.remove(c);
        }
        assertEquals(0,expectedSet.size());

    }

    @Test
    void getCoordinateByRelativeIndex(){
        RectangleShip<Character> rectangleShip = new RectangleShip<>("test 3*2 ship",new Coordinate(0, 0), 3, 2, 's', '*');
        HashSet<Coordinate> expectedSet = new HashSet<>();
        expectedSet.add(new Coordinate(0, 0));
        expectedSet.add(new Coordinate(0, 1));
        expectedSet.add(new Coordinate(0, 2));
        expectedSet.add(new Coordinate(1, 0));
        expectedSet.add(new Coordinate(1, 1));
        expectedSet.add(new Coordinate(1, 2));
        for (Coordinate c : rectangleShip.getCoordinates()) {
            assertTrue(expectedSet.contains(c));
            expectedSet.remove(c);
        }
        assertEquals(0,expectedSet.size());
        assertNull(rectangleShip.getCoordinateByRelativeIndex(-1));
    }
}