package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoCollisionRuleCheckerTest {

    @Test
    void checkMyRule() {

        NoCollisionRuleChecker<Character> noCollisionRuleChecker = new NoCollisionRuleChecker<>(null);
        InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(noCollisionRuleChecker);
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');


        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2_2 = new Placement(new Coordinate(2, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        Ship<Character> dst2 = f.makeSubmarine(v1_2);
        Ship<Character> dst3 = f.makeSubmarine(v2_2);

        assertNull(noCollisionRuleChecker.checkMyRule(dst,b1),"");

        b1.tryAddShip(dst);

        assertEquals(noCollisionRuleChecker.checkMyRule(dst2,b1),"That placement is invalid: the ship overlaps another ship.");
        assertEquals(noCollisionRuleChecker.checkMyRule(dst3,b1),"That placement is invalid: the ship overlaps another ship.");



        assertEquals(inBoundsRuleChecker.checkPlacement(dst3,b1),"That placement is invalid: the ship overlaps another ship.");



    }
}