package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InBoundsRuleCheckerTest {

    @Test
    void checkMyRule() {
        InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');


        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        assertNull(inBoundsRuleChecker.checkPlacement(dst,b1));

        Ship<Character> dst1 = new V1ShipFactory().makeSubmarine(new Placement(new Coordinate(-1, 2), 'V'));
        assertEquals(inBoundsRuleChecker.checkPlacement(dst1,b1),"That placement is invalid: the ship goes off the top of the board.");

        Ship<Character> dst2 = new V1ShipFactory().makeSubmarine(new Placement(new Coordinate(1, -2), 'V'));
        assertEquals(inBoundsRuleChecker.checkPlacement(dst2,b1),"That placement is invalid: the ship goes off the left of the board.");

        Ship<Character> dst3 = new V1ShipFactory().makeSubmarine(new Placement(new Coordinate(1, 10), 'V'));
        assertEquals(inBoundsRuleChecker.checkPlacement(dst3,b1),"That placement is invalid: the ship goes off the right of the board.");


        Ship<Character> dst4 = new V1ShipFactory().makeSubmarine(new Placement(new Coordinate(20, 2), 'V'));
        assertEquals(inBoundsRuleChecker.checkPlacement(dst4,b1),"That placement is invalid: the ship goes off the bottom of the board.");


    }
}