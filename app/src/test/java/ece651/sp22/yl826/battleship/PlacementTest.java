package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {

    @Test
    public void test_where_and_orientation() {
        Placement p1 = new Placement(new Coordinate("A0"), 'V');
        assertEquals('V', p1.getOrientation());
        assertEquals(0, p1.getWhere().getColumn());
        assertEquals(0, p1.getWhere().getRow());
    }

    @Test
    public void test_equals() {
        Placement c1 = new Placement(new Coordinate("A0"), 'V');
        Placement c2 = new Placement(new Coordinate("A0"), 'V');
        Placement c3 = new Placement(new Coordinate("A0"), 'E');
        Placement c4 = new Placement(new Coordinate("C5"), 'W');
        Placement c5 = new Placement(new Coordinate("C5"), 'V');
        Placement c6 = new Placement(new Coordinate("C6"), 'W');
        assertEquals(c1, c1);   //equals should be reflexive
        assertEquals(c1, c2);   //different objects be same contents
        assertNotEquals(c1, c3);  //different contents
        assertNotEquals(c1, c4);
        assertNotEquals(c3, c4);
        assertNotEquals(c4, c5);
        assertNotEquals(c4, c6);
        assertNotEquals(c1, "Placement{where=(0, 0), orientation=V}"); //different types
    }

    @Test
    public void test_hashCode() {
        Placement c1 = new Placement(new Coordinate("A0"), 'V');
        Placement c2 = new Placement(new Coordinate("A0"), 'V');
        Placement c3 = new Placement(new Coordinate("A0"), 'E');
        Placement c4 = new Placement(new Coordinate("C5"), 'W');
        assertEquals(c1.hashCode(), c2.hashCode());
        assertNotEquals(c1.hashCode(), c3.hashCode());
        assertNotEquals(c1.hashCode(), c4.hashCode());
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement c1 = new Placement("B3V");
        assertEquals(1, c1.getWhere().getRow());
        assertEquals(3, c1.getWhere().getColumn());
        Placement c2 = new Placement("D5v");
        assertEquals(3, c2.getWhere().getRow());
        assertEquals(5, c2.getWhere().getColumn());
        Placement c3 = new Placement("A9V");
        assertEquals(0, c3.getWhere().getRow());
        assertEquals(9, c3.getWhere().getColumn());
        Placement c4 = new Placement("Z0V");
        assertEquals(25, c4.getWhere().getRow());
        assertEquals(0, c4.getWhere().getColumn());
    }

    @Test
    public void test_toString() {
        Placement c1 = new Placement(new Coordinate("A0"), 'V');
        assertNotEquals(c1, "Placement{where=(0, 0), orientation=V}");
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("A00"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0AA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("00"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1B0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0]"));
    }
}