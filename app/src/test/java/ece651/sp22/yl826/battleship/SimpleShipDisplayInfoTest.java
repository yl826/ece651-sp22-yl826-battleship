package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {

    @Test
    void getInfo() {
        char normal = 's';
        char onHit = 'x';
        SimpleShipDisplayInfo<Character> s = new SimpleShipDisplayInfo<>(normal,onHit);
        assertEquals(onHit,s.getInfo(new Coordinate(0, 0), true));
        assertEquals(normal,s.getInfo(new Coordinate(0, 0), false));
    }
}