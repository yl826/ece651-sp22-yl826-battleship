package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BoardTextViewTest {
    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<>(2, 2, 'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected =
                expectedHeader +
                        "A  |  A\n" +
                        "B  |  B\n" +
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10, 27, 'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_display_empty_3by2() {
        emptyVoardHelper(3, 2, "  0|1|2\n", "A  | |  A\nB  | |  B\n");
    }

    @Test
    public void test_display_empty_2by3() {
        emptyVoardHelper(2, 3, "  0|1\n", "A  |  A\nB  |  B\nC  |  C\n");
    }

    private void emptyVoardHelper(int width, int height, String expectedHeader, String expectedBody) {
        Board<Character> b1 = new BattleShipBoard<>(width, height, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    private void generalVoardHelper(Board<Character> b1, String expectedHeader, String expectedBody) {
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_not_empty_4by3() {
        BattleShipBoard<Character> b = new BattleShipBoard<>(4, 3, 'X');
        generalVoardHelper(b, "  0|1|2|3\n", "A  | | |  A\nB  | | |  B\nC  | | |  C\n");


        b.tryAddShip(new RectangleShip<>(new Coordinate(0, 0), 's', '*'));
        generalVoardHelper(b, "  0|1|2|3\n", "A s| | |  A\nB  | | |  B\nC  | | |  C\n");

        b.tryAddShip(new RectangleShip<>(new Coordinate(0, 1), 's', '*'));
        generalVoardHelper(b, "  0|1|2|3\n", "A s|s| |  A\nB  | | |  B\nC  | | |  C\n");


    }

    @Test
    void displayEnemyBoard() {
        BattleShipBoard<Character> b = new BattleShipBoard<>(4, 3, 'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement h10 = new Placement(new Coordinate(1, 0), 'H');
        Placement v03 = new Placement(new Coordinate(0, 3), 'V');
        Ship<Character> sub = f.makeSubmarine(h10);
        Ship<Character> dst = f.makeDestroyer(v03);

        //Add new ship to the board
        b.tryAddShip(sub);
        b.tryAddShip(dst);
        BoardTextView bv = new BoardTextView(b);
        String myView =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        String enemyView =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B  | | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        //make sure we laid things out the way we think we did.
        assertEquals(myView, bv.displayMyOwnBoard());
        assertEquals(enemyView, bv.displayEnemyBoard());
        b.fireAt(new Coordinate(1, 0));
        enemyView =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        assertEquals(enemyView, bv.displayEnemyBoard());

        b.fireAt(new Coordinate(0, 0));
        enemyView =
                        "  0|1|2|3\n" +
                        "A X| | |  A\n" +
                        "B s| | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        assertEquals(enemyView, bv.displayEnemyBoard());
    }

    @Test
    public void displayMyBoardWithEnemyNextToIt(){

        Board<Character> b = new BattleShipBoard<Character>(3, 3, 'X');
        BoardTextView bc = new BoardTextView(b);
        Board<Character> enemyBoard = new BattleShipBoard<Character>(3, 3, 'X');
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String expected =
                "     Your ocean             Player B's ocean\n"+
                        "  0|1|2                    0|1|2\n"+
                        "A  | |  A                A  | |  A\n"+
                        "B  | |  B                B  | |  B\n"+
                        "C  | |  C                C  | |  C\n"+
                        "  0|1|2                    0|1|2\n";

        assertEquals(expected, bc.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", "Player B's ocean"));
        V1ShipFactory f = new V1ShipFactory();
        Placement V00 = new Placement("A0V");
        b.tryAddShip(f.makeSubmarine(V00));

        Placement H10 = new Placement("B0H");
        enemyBoard.tryAddShip(f.makeSubmarine(H10));
        enemyBoard.fireAt(new Coordinate(1, 0));
        enemyBoard.fireAt(new Coordinate(2, 0));


        expected =
                "     Your ocean             Player B's ocean\n"+
                        "  0|1|2                    0|1|2\n"+
                        "A s| |  A                A  | |  A\n"+
                        "B s| |  B                B s| |  B\n"+
                        "C  | |  C                C X| |  C\n"+
                        "  0|1|2                    0|1|2\n";
        assertEquals(expected, bc.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", "Player B's ocean"));
    }





}