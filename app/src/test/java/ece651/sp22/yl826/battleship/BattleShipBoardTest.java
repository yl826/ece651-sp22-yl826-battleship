package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<>(10, 20,'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10,-5,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8,20,'X'));

    }

    @Test
    public void test_emptyBoard(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');
        Character[][] expected =  new Character[10][20];
        assertThrows(IllegalArgumentException.class, ()->b1.whatIsAt(new Coordinate(9,21),true));
        assertThrows(IllegalArgumentException.class, ()->b1.whatIsAt(new Coordinate(21,9),true));
        assertThrows(IllegalArgumentException.class, ()->b1.whatIsAt(new Coordinate(21,21),true));
        //checkWhatIsAtBoard(b1,expected);
        assertNull(b1.getShipAt(new Coordinate(0,0)));
        assertNull(b1.tryAddShip(new RectangleShip<>(new Coordinate(0, 0),'s','*')));
        assertNull(b1.getShipAt(new Coordinate(2,1)));
        expected[0][0] = 's';
        //checkWhatIsAtBoard(b1,expected);
    }

    @Test
    void tryAddShip() {
        NoCollisionRuleChecker<Character> noCollisionRuleChecker = new NoCollisionRuleChecker<>(null);
        InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(noCollisionRuleChecker);
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');


        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2_2 = new Placement(new Coordinate(2, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        Ship<Character> dst2 = f.makeSubmarine(v1_2);
        Ship<Character> dst3 = f.makeSubmarine(v2_2);

        assertNull(b1.tryAddShip(dst));
        assertNotNull(b1.tryAddShip(dst2));
        assertNotNull(b1.tryAddShip(dst3));
    }

    @Test
    void fireAt() {
        NoCollisionRuleChecker<Character> noCollisionRuleChecker = new NoCollisionRuleChecker<>(null);
        InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(noCollisionRuleChecker);
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');

        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2_2 = new Placement(new Coordinate(2, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);

        //Add new ship to the board
        b1.tryAddShip(dst);

        assertThrows(IllegalArgumentException.class, ()->b1.fireAt(new Coordinate(-1,-1)));
        //check if return the same ship
        assertSame( dst, b1.fireAt(new Coordinate(1, 2)));
        //check the side effect
        assertFalse(dst.isSunk());

        b1.fireAt(new Coordinate(2, 2));

        assertTrue(dst.isSunk());

        assertNull(b1.fireAt(new Coordinate(3, 2)));
    }

    @Test
    void whatIsAtForEnemy(){
        NoCollisionRuleChecker<Character> noCollisionRuleChecker = new NoCollisionRuleChecker<>(null);
        InBoundsRuleChecker<Character> inBoundsRuleChecker = new InBoundsRuleChecker<>(noCollisionRuleChecker);
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');

        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2_2 = new Placement(new Coordinate(2, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);

        //Add new ship to the board
        b1.tryAddShip(dst);
        //check if return the same ship
        b1.fireAt(new Coordinate(0, 0));
        assertEquals('X',b1.whatIsAtForEnemy(new Coordinate(0, 0)));

        b1.fireAt(new Coordinate(1, 2));
        assertEquals('s',b1.whatIsAtForEnemy(new Coordinate(1, 2)));

        b1.fireAt(new Coordinate(0, 0));
        assertEquals('X',b1.whatIsAtForEnemy(new Coordinate(0, 0)));
    }

    @Test
    void isAllSunk(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20,'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement h10 = new Placement("A0H");
        Placement v12 = new Placement("B2V");

        b1.tryAddShip(f.makeSubmarine(h10));
        b1.tryAddShip(f.makeSubmarine(v12));

        b1.fireAt(new Coordinate(0,0));
        b1.fireAt(new Coordinate(0, 1));
        assertFalse(b1.isAllSunk());

        b1.fireAt(new Coordinate(1, 2));
        b1.fireAt(new Coordinate(2, 2));
        b1.isAllSunk();
        assertTrue(b1.isAllSunk());

    }


    @Test
    void getShipAt(){
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 8, 'X');
        V2ShipFactory f = new V2ShipFactory();
        Placement p1 = new Placement("G0R");
        Placement p2 = new Placement("A0U");
        // Ship<Character> s1 = f.makeSubmarine(p1);
        Ship<Character> s2 = f.makeCarrier(p2);
        Ship<Character> s3 = f.makeCarrier(p1);
        assertNull(b.tryAddShip(s2));
        b.fireAt(new Coordinate(1, 0));
        b.fireAt(new Coordinate(2, 1));
        assertNotNull(b.getShipAt(new Coordinate(0,0)));
    }


    @Test
    void tryMoveShipRuleCheck(){
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 8, 'X');
        V2ShipFactory f = new V2ShipFactory();
        Placement p1 = new Placement("G0R");
        Placement p2 = new Placement("A0U");
        Placement p3 = new Placement("H0H");
        Placement p4 = new Placement("Y0H");
        // Ship<Character> s1 = f.makeSubmarine(p1);
        Ship<Character> s2 = f.makeCarrier(p2);
        Ship<Character> s3 = f.makeCarrier(p1);
        Ship<Character> blockShip = f.makeSubmarine(p3);
        Ship<Character> outBoundShip = f.makeSubmarine(p4);
        b.tryAddShip(s2);
        b.fireAt(new Coordinate(1, 0));
        b.fireAt(new Coordinate(2, 1));
        BoardTextView view = new BoardTextView(b);

        b.tryAddShip(blockShip);

        assertThrows(IllegalArgumentException.class,()->b.tryMoveShip(s3,s2));
        assertThrows(IllegalArgumentException.class,()->b.tryMoveShip(outBoundShip,s2));
        assertEquals("  0|1|2|3|4|5|6|7\n" +
                "A c| | | | | | |  A\n" +
                "B *| | | | | | |  B\n" +
                "C c|*| | | | | |  C\n" +
                "D c|c| | | | | |  D\n" +
                "E  |c| | | | | |  E\n" +
                "F  | | | | | | |  F\n" +
                "G  | | | | | | |  G\n" +
                "H s|s| | | | | |  H\n" +
                "  0|1|2|3|4|5|6|7\n",view.displayMyOwnBoard());
    }

    @Test
    void tryMoveShip(){
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(8, 8, 'X');
        V2ShipFactory f = new V2ShipFactory();
        Placement p1 = new Placement("G0R");
        Placement p2 = new Placement("A0U");
        Placement p3 = new Placement("H0H");
        // Ship<Character> s1 = f.makeSubmarine(p1);
        Ship<Character> s2 = f.makeCarrier(p2);
        Ship<Character> s3 = f.makeCarrier(p1);
        Ship<Character> blockShip = f.makeSubmarine(p3);
        b.tryAddShip(s2);
        b.fireAt(new Coordinate(1, 0));
        b.fireAt(new Coordinate(2, 1));
        BoardTextView view = new BoardTextView(b);

        //b.tryAddShip(blockShip);
        assertEquals("  0|1|2|3|4|5|6|7\n" +
                "A c| | | | | | |  A\n" +
                "B *| | | | | | |  B\n" +
                "C c|*| | | | | |  C\n" +
                "D c|c| | | | | |  D\n" +
                "E  |c| | | | | |  E\n" +
                "F  | | | | | | |  F\n" +
                "G  | | | | | | |  G\n" +
                "H  | | | | | | |  H\n" +
                "  0|1|2|3|4|5|6|7\n",view.displayMyOwnBoard());
        b.tryMoveShip(s3,s2);
        assertEquals("  0|1|2|3|4|5|6|7\n" +
                "A  | | | | | | |  A\n" +
                "B  | | | | | | |  B\n" +
                "C  | | | | | | |  C\n" +
                "D  | | | | | | |  D\n" +
                "E  | | | | | | |  E\n" +
                "F  | | | | | | |  F\n" +
                "G  |c|c|*|c| | |  G\n" +
                "H c|c|*| | | | |  H\n" +
                "  0|1|2|3|4|5|6|7\n",view.displayMyOwnBoard());
    }

    @Test
    public void test_sonar() {
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 20, 'X');
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> submarine = factory.makeSubmarine(new Placement(new Coordinate(0,0), 'V'));
        Ship<Character> destroyer = factory.makeDestroyer(new Placement(new Coordinate(2,4), 'V'));
        Ship<Character> battleship = factory.makeBattleship(new Placement(new Coordinate( 7,3), 'U'));
        Ship<Character> carrier = factory.makeCarrier(new Placement(new Coordinate(10,2),  'L'));
        b.tryAddShip(submarine);
        b.tryAddShip(destroyer);
        b.tryAddShip(battleship);
        b.tryAddShip(carrier);

        BoardTextView view = new BoardTextView(b);
        //assertEquals("", view.displayMyOwnBoard());

        Coordinate center = new Coordinate("G3");
        HashMap<String, Integer> count = b.sonar(3, center);
        HashMap<String, Integer> expected = new HashMap<String, Integer>();
        expected.put("Submarine", 0);
        expected.put("Destroyer", 1);
        expected.put("Battleship", 3);
        expected.put("Carrier", 0);
        assertEquals(expected, count);

        assertThrows(IllegalArgumentException.class, () -> b.sonar(3, new Coordinate(100, 3)));
        assertThrows(IllegalArgumentException.class, () -> b.sonar(3, new Coordinate(3,100)));
    }


}