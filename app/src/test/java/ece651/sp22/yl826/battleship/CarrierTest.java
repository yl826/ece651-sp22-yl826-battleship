package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CarrierTest {

    @Test
    void makeCoords() {
        Set<Coordinate> c = Carrier.makeCoords(new Coordinate(0, 0), 'U').keySet();
        assertEquals(c, new HashSet<Coordinate>() {{
            add(new Coordinate(0, 0));
            add(new Coordinate(1, 0));
            add(new Coordinate(2, 0));
            add(new Coordinate(3, 0));
            add(new Coordinate(2, 1));
            add(new Coordinate(3, 1));
            add(new Coordinate(4, 1));
        }});

        c = Carrier.makeCoords(new Coordinate(0, 0), 'R').keySet();
        assertEquals(c, new HashSet<Coordinate>() {{
            add(new Coordinate(0, 1));
            add(new Coordinate(0, 2));
            add(new Coordinate(0, 3));
            add(new Coordinate(0, 4));
            add(new Coordinate(1, 0));
            add(new Coordinate(1, 1));
            add(new Coordinate(1, 2));
        }});

        c = Carrier.makeCoords(new Coordinate(0, 0), 'D').keySet();
        assertEquals(c, new HashSet<Coordinate>() {{
            add(new Coordinate(0, 0));
            add(new Coordinate(1, 0));
            add(new Coordinate(2, 0));
            add(new Coordinate(1, 1));
            add(new Coordinate(2, 1));
            add(new Coordinate(3, 1));
            add(new Coordinate(4, 1));
        }});

        c = Carrier.makeCoords(new Coordinate(0, 0), 'L').keySet();
        assertEquals(c, new HashSet<Coordinate>() {{
            add(new Coordinate(0, 2));
            add(new Coordinate(0, 3));
            add(new Coordinate(0, 4));
            add(new Coordinate(1, 0));
            add(new Coordinate(1, 1));
            add(new Coordinate(1, 2));
            add(new Coordinate(1, 3));
        }});


    }
    @Test
    public void getName(){
        Placement p1 = new Placement("C0L");
        Carrier<Character> sU = new Carrier<>("testship", p1.getWhere(), 's', '*',p1.getOrientation());
        assertEquals("testship",sU.getName());
        assertEquals("[(3, 3), (2, 3), (3, 1), (2, 2), (3, 2), (2, 4), (3, 0)]",sU.getCoordinates().toString());
    }

}