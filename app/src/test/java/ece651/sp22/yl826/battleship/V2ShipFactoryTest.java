package ece651.sp22.yl826.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class V2ShipFactoryTest {

    @Test
    void makeSubmarine() {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        checkShip(dst, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));
        assertThrows(IllegalArgumentException.class,()->f.makeSubmarine(new Placement(new Coordinate(1, 2), 'U')));

    }

    @Test
    void makeBattleship() {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');

        try {
            Ship<Character> dst = f.makeBattleship(v1_2);
        }catch (IllegalArgumentException e){
            assertEquals("Invalid orientation for BattleShip: V",e.getMessage());
        }


    }

    @Test
    void makeCarrier() {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        try {
            Ship<Character> dst = f.makeCarrier(v1_2);
        }catch (IllegalArgumentException e){
            assertEquals("Invalid orientation for Carrier: V",e.getMessage());
        }
        f.makeCarrier(new Placement(new Coordinate(1, 2), 'D'));

    }

    @Test
    void makeDestroyer() {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    }

    @Test
    void createShip() {
    }

    /**
     * Check all coordinates and ship name.
     *
     * @param testShip       is the ship need to check.
     * @param expectedName   is the expected name of the ship.
     * @param expectedLetter is the expected display letter of the ship.
     * @param expectedLocs   is an array of the expected locations of this ship.
     */
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs)
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));

    }
}