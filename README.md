# ece651-sp22-yl826-battleship

[![pipeline status](https://gitlab.oit.duke.edu/yl826/testforcicd/badges/main/pipeline.svg)](https://yl826.pages.oit.duke.edu/ece651-sp22-yl826-battleship/dashboard.html)
[![coverage report](https://gitlab.oit.duke.edu/yl826/testforcicd/badges/main/coverage.svg)](https://yl826.pages.oit.duke.edu/ece651-sp22-yl826-battleship/dashboard.html)

## Coverage
[Detailed coverage](https://yl826.pages.oit.duke.edu/ece651-sp22-yl826-battleship/dashboard.html)
