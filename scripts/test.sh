#!/bin/bash

./gradlew build || exit 1
./gradlew cloverGenerateReport || exit 1
scripts/coverage_summary.sh
ls -l /
ls -l /coverage-out/
ls -l app/build/reports/clover/
cd app/build/reports/clover/
php /home/juser/scripts/clover-to-cobertura.php < clover.xml > cobertura.xml
cd /home/juser/
cp -r app/build/reports/clover/cobertura.xml /coverage-out/
cp -r app/build/reports/clover/html/* /coverage-out/ || exit 1

