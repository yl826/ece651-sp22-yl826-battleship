import React, {Component} from "react";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import axios from "axios";
import authHeader from "../services/auth-header";
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";

const API_URL = "http://localhost:8080/api/game/";


/**
 * handle the action of placing units from user
 */
export default class unitPlace extends Component {
    /**
     * ctor
     * @param props
     */
    constructor(props) {
        super(props);

        this.handleCommit = this.handleCommit.bind(this);
        this.state = {
            loading: true,
            message: "",
            roomID: this.props.roomID,
            rows: [], //this.initComponent = this.initComponent.bind(this);
            openConfirmDialog: false
        };
    }

    /**
     * add default values for fields
     */
    componentDidMount() {
        let idCounter = 0;
        let tmpRows = [];
        tmpRows.push({
            id: idCounter++,
            shipType: "Submarine",
            row: 'A',
            col: '0',
            ori: 'H'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Submarine",
            row: 'B',
            col: '0',
            ori: 'V'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Destroyer",
            row: 'A',
            col: '2',
            ori: 'H'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Destroyer",
            row: 'B',
            col: '2',
            ori: 'H'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Destroyer",
            row: 'C',
            col: '2',
            ori: 'H'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Battleship",
            row: 'D',
            col: '0',
            ori: 'U'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Battleship",
            row: 'F',
            col: '0',
            ori: 'U'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Battleship",
            row: 'H',
            col: '0',
            ori: 'U'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Carrier",
            row: 'D',
            col: '5',
            ori: 'U'
        })
        tmpRows.push({
            id: idCounter++,
            shipType: "Carrier",
            row: 'D',
            col: '7',
            ori: 'U'
        })
        // for (const territory of this.props.room.riskMap.continent) {
        //     if (territory.ownerID === AuthService.getCurrentUser().id) tmpRows.push({
        //         id: idCounter++, territoryName: territory.name, unitAmount: null
        //     })
        //
        // }
        this.setState({rows: tmpRows});


    }

    /**
     * handle the commit of some cell
     * @param id
     */
    handleCellCommit = (id) => {


        console.log(id)
        this.setState(prevState => ({

            rows: prevState.rows.map(el =>{
                const res = Object.assign({}, el)
                if(el.id === id.id){
                    res[id.field] = id.value
                }
                return res
            })

        }))

    }

    /**
     * render the elements in this page
     * @returns {JSX.Element}
     */
    render() {
        console.log(this.props.room)
        let columns: GridColDef[] = [
            {field: 'id', headerName: 'ID', width: 90},
            {field: 'shipType', headerName: 'Ship Type', width: 150, editable: false,},
            {
                field: 'row', headerName: 'Row Number', width: 150, editable: true, type: "singleSelect",
                valueOptions: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T']
            },
            {
                field: 'col', headerName: 'Column Number', width: 150, editable: true, type: "singleSelect",
                valueOptions: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            },
            {
                field: 'ori', headerName: 'Orientation ', width: 150, editable: true, type: "singleSelect",
                valueOptions: ({row}) => {
                    if (row.shipType === "submarine" || row.shipType === "destroyer") {
                        return ['V', 'H'];
                    }
                    return ['Up', 'Right', 'Down', 'Left'];
                }
            },
        ];

        return (

            <div style={{height: 400, width: '100%'}}>
                <p>you are going to place the following ships (which are all
                    rectangular). For each ship, type the coordinate of the upper left
                    side of the ship, followed by either H (for horizontal) or V (for
                    vertical). For example M4H would place a ship horizontally starting
                    at M4 and going to the right. You have
                </p>
                <p>2 "Submarines" ships that are 1x2</p>
                <p>3 "Destroyers" that are 1x3</p>
                <p>3 "Battleships" that are 1x4</p>
                <p>2 "Carriers" that are 1x6</p>
                <DataGrid

                    rows={this.state.rows}
                    columns={columns}
                    // pageSize={5}
                    // rowsPerPageOptions={[5]}
                    // experimentalFeatures={{newEditingApi: true}}
                    onCellEditCommit={this.handleCellCommit}
                />
                <Button variant="contained" onClick={this.handleConfirmDialogOpen}>Commit</Button>
                <Dialog

                    open={this.state.openConfirmDialog}

                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">
                        {"Confirm Placement?"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Are you sure?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.handleCommit}>
                            Commit
                        </Button>
                        <Button onClick={this.handleConfirmDialogClose} autoFocus>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>

            </div>);
    }


    initComponent() {


    }

    /**
     * open the confirm dialog ask for confirmation from user
     */
    handleConfirmDialogOpen = () => {
        this.setState({openConfirmDialog: true})
    }
    /**
     * close the confirm dialog
     */
    handleConfirmDialogClose = () => {
        this.setState({openConfirmDialog: false})
    }

    /**
     * handle the commit of all placing unit orders
     * @param e
     */
    handleCommit(e) {
        console.log(this.ref)
        console.log(this.state.rows)
        let shipPlaceOrders = [];
        for (const row of this.state.rows) {
            shipPlaceOrders.push({
                "shipName": row.shipType,
                "orientation": row.ori,
                "row": row.row,
                "column": row.col
            })

        }
        console.log(shipPlaceOrders);
        axios
            .post("/api/game/place/ship", {
                roomID: this.state.roomID, shipPlaceOrders
            }, {headers: authHeader()})
            .then((response) => {
                console.log(response);
                //this.setState({messages: tmpmessage});
                this.props.handleSnackBarUpdate("success", response.data.prompt)
            }).catch((error) => {
            console.log(error.response);
            this.props.handleSnackBarUpdate("error", error.response.data.prompt);
            this.setState({messages: error});
        });

    }


    componentDidUpdate() {
        this.initComponent();
    }


}