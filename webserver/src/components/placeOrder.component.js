import React, {Component} from "react";
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import axios from "axios";
import authHeader from "../services/auth-header";
import Box from '@mui/material/Box';
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Stack} from "@mui/material";
import AuthService from "../services/auth.service";


/**
 * handle the acitons of placing orders
 */
class unitPlace extends Component {
    /**
     * ctor
     * @param props
     */
    constructor(props) {
        super(props);

        this.handleCommit = this.handleCommit.bind(this);
        this.state = {
            loading: true,
            message: "",
            roomID: this.props.roomID,
            rows: [], //this.initComponent = this.initComponent.bind(this);
            idCounter: 1,
            columns: [],
            selectedRowsID: []
        };
    }

    /**
     * set the default values or choosable value for the grid cell data
     */
    componentDidMount() {
        let allTerritory = [];
        let enemyTerritory = [];
        let ownTerritory = [];
        let myUnitTypes = [];
        // for (const territory of this.props.room.riskMap.continent) {
        //     allTerritory.push(territory.name);
        //     if (territory.ownerID === AuthService.getCurrentUser().id) {
        //         ownTerritory.push(territory.name);
        //         for (let i = 0; i < 7; i++) {
        //             let unitInfo = "Soldier" + " level " + i;
        //             if (!myUnitTypes.includes(unitInfo)) {
        //                 myUnitTypes.push(unitInfo);
        //             }
        //         }
        //     } else {
        //         enemyTerritory.push(territory.name);
        //     }
        // }
        this.setState(prevState => ({
            columns: [
                {field: 'id', headerName: 'ID', width: 30},
                {
                    field: 'orderType',
                    headerName: 'Order Type',
                    width: 100,
                    editable: true,
                    type: "singleSelect",
                    valueOptions: ["Move", "Fire", "Sonar Scan"]
                },
                {
                    field: 'row', headerName: 'Target Row', width: 120, editable: true, type: "singleSelect",
                    valueOptions: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T']
                },
                {
                    field: 'col', headerName: 'Target Column', width: 120, editable: true, type: "singleSelect",
                    valueOptions: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
                },
                {
                    field: 'ori', headerName: 'Target Orientation(When Move)', width: 230, editable: true, type: "singleSelect",
                    valueOptions: ({row}) => {
                        return ['Up', 'Right', 'Down', 'Left','V','H'];
                    }
                },
                {
                    field: 'source_row', headerName: 'Source Row(When Move)', width: 200, editable: true, type: "singleSelect",
                    valueOptions: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T']
                },
                {
                    field: 'source_col', headerName: 'Source Column(When Move)', width: 200, editable: true, type: "singleSelect",
                    valueOptions: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
                },
            ]
        }))

    }


    /**
     * handle the select of some row
     * @param selection
     */
    handleSelect = (selection) => {
        //console.log(selection);
        this.setState({selectedRowsID: selection})
    }

    /**
     * handle add one row
     */
    handleAddRow = () => {
        this.setState(prevState => ({
            rows: [...prevState.rows, {
                id: prevState.idCounter,
                oderType: null,
                row: null,
                col: null,
                ori: null,
                source_col: null,
                source_row: null
            }],
            idCounter: prevState.idCounter + 1
        }))
    }

    /**
     * handle delete some selected row
     */
    handleDeleteSelectedRows = () => {
        this.setState(prevState => ({

            rows: prevState.rows.filter(el => !prevState.selectedRowsID.includes(el.id))

        }))
    }

    /**
     * render all the elements in the page
     * @returns {JSX.Element}
     */
    render() {
        //console.log(this.props.room)
        let columns: GridColDef[] = this.state.columns;

        return (
            <div style={{
                height: 400,
                width: '100%'
            }}>
                <Stack
                    sx={{width: '100%', mb: 1}}
                    direction="row"
                    alignItems="flex-start"
                    columnGap={1}

                ><Button variant="contained" size="small" color="success" onClick={this.handleAddRow}>
                    Add a order
                </Button>
                    <Button variant="contained" color="error" size="small"
                            onClick={this.handleDeleteSelectedRows}>

                        Delete selected rows
                    </Button>

                </Stack>
                <Box sx={{height: 400, bgcolor: 'background.paper'}}>
                    <DataGrid
                        checkboxSelection
                        hideFooter
                        rows={this.state.rows}
                        onSelectionModelChange={this.handleSelect}
                        onCellEditCommit={this.handleRowCommit}
                        columns={columns}/>
                </Box>

                <Button variant="contained" onClick={this.handleConfirmDialogOpen}>Commit</Button>
                <Dialog

                    open={this.state.openConfirmDialog}

                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">
                        {"Confirm Placement?"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Are you sure?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={this.handleCommit}>
                            Commit
                        </Button>
                        <Button onClick={this.handleConfirmDialogClose} autoFocus>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

    handleRowCommit = (row) => {
        console.log(row);
        this.setState(prevState => ({

            rows: prevState.rows.map(el => el.id === row.id ? {...el, [row.field]: row.value} : el)

        }))

    }

    initComponent() {


    }

    handleCommit(e) {

        let orders = [];
        for (const row of this.state.rows) {
            orders.push({

                orderType: row.orderType,
                row: row.row,
                col: row.col,
                ori: row.ori,
                source_row: row.source_row,
                source_col: row.source_col
            })
        }
        console.log(orders);
        axios
            .post("/api/game/place/order", {
                roomID: this.state.roomID,
                orders
            }, {headers: authHeader()})
            .then((response) => {
                    console.log(response);
                    this.props.handleSnackBarUpdate("success", response.data.prompt)
                    this.setState({
                        rows: []
                    })
                    this.setState({openConfirmDialog: false})
                    //this.setState({messages: tmpmessage});
                    //window.location.reload();
                }
            ).catch((error) => {
            console.log(error);
            this.props.handleSnackBarUpdate("error", error.response.data.prompt);
            this.setState({messages: error});
            console.log(error.response);
        });

    }

    handleConfirmDialogOpen = () => {
        this.setState({openConfirmDialog: true})
    }
    handleConfirmDialogClose = () => {
        this.setState({openConfirmDialog: false})
    }

    componentDidUpdate() {
        this.initComponent();
    }


}


export default unitPlace;

